package com.clickpro.adapter;

import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.clickpro.ccu.R;
import com.clickpro.entity.Property;

public class SettingAdapter extends BaseAdapter {
	private Context context;
	private List<Property> lists;
	SharedPreferences pref;
	Editor editor;
	String name;

	public SettingAdapter(Context context, List<Property> lists) {
		// TODO Auto-generated constructor stub
		this.lists = lists;
		this.context = context;
		pref = context.getSharedPreferences("MyPref", 0);
		editor = pref.edit();

		name = pref.getString("ACCESS_KEY", "");

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return lists.size() > 0 ? lists.size() : 0;
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return lists.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(int arg0, View convertView, ViewGroup arg2) {
		// TODO Auto-generated method stub
		Item item = null;
		if (convertView == null) {
			item = new Item();
			convertView = LayoutInflater.from(context).inflate(
					R.layout.setting_item, null);
			item.itemName = (TextView) convertView.findViewById(R.id.itemName);
			item.value = (TextView) convertView.findViewById(R.id.value);
			item.check = (ImageView) convertView.findViewById(R.id.rightArrow);
			convertView.setTag(item);
		} else {
			item = (Item) convertView.getTag();
		}
		item.itemName.setText(lists.get(arg0).getName());

		if (lists.get(arg0).getValue() != null) {
			item.value.setText(lists.get(arg0).getValue());
		} else if (lists.get(arg0).getValue() == "Sync with Dropbox") {
			if (!name.equals("")) {
				item.check.setImageResource(R.drawable.tick);
			} else {
				item.check.setImageResource(R.drawable.right_arrow);
			}

		}

		return convertView;
	}

	class Item {
		TextView itemName, value;
		ImageView check;
	}
}
