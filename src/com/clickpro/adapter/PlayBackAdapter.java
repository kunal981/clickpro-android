package com.clickpro.adapter;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.clickpro.ccu.DownloadManager;
import com.clickpro.ccu.DownloadService;
import com.clickpro.ccu.R;
import com.clickpro.entity.DownloadInfo;
import com.clickpro.util.CommonUtil;
import com.lidroid.xutils.exception.DbException;

public class PlayBackAdapter extends BaseAdapter {
	private Context context;
	private ArrayList<String> lists;
	Item item = null;
	String storePath = Environment.getExternalStorageDirectory()
			+ "/ClickProPolar/";

	String filename;

	public PlayBackAdapter(Context context, ArrayList<String> lists) {
		// TODO Auto-generated constructor stub
		this.lists = lists;
		this.context = context;

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return lists.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return lists.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(final int arg0, View convertView, ViewGroup arg2) {
		// TODO Auto-generated method stub
		if (convertView == null) {
			item = new Item();
			convertView = LayoutInflater.from(context).inflate(
					R.layout.playback_item, null);
			item.fileName = (TextView) convertView.findViewById(R.id.fileName);
			item.downloadBtn = (Button) convertView
					.findViewById(R.id.downloadBtn);
			convertView.setTag(item);
		} else {
			item = (Item) convertView.getTag();
		}
		final String fName = getFileName(lists.get(arg0));
		item.fileName.setText(fName);
		item.downloadBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				final DownloadManager downloadManager = DownloadService
						.getDownloadManager(context.getApplicationContext());
				final String downloadUrl = lists.get(arg0);
				filename = getFileName(downloadUrl);
				try {
					final DownloadInfo downloadInfo = downloadManager
							.isExit(downloadUrl);
					if (downloadInfo != null) {
						AlertDialog.Builder builder = new AlertDialog.Builder(
								context);
						builder.setTitle(context.getString(R.string.prompt));
						builder.setMessage(context
								.getString(R.string.file_exists));
						builder.setPositiveButton(
								context.getString(R.string.redownload),
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
										dialog.dismiss();
										CommonUtil
												.showToast(
														context,
														"\""
																+ fName
																+ "\""
																+ " has been added to the download queue");
										filename = new SimpleDateFormat(
												"yyyyMMdd_HHmmss")
												.format(new Date())
												+ ".JPG";
										try {
											downloadManager.addNewDownload(
													downloadUrl, filename,
													storePath + filename, true,
													true, null);

										} catch (DbException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									}
								});
						builder.setNegativeButton(
								context.getString(R.string.cancel),
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
										dialog.dismiss();
									}
								});
						builder.show();
					} else {
						CommonUtil.showToast(context, "\"" + fName + "\""
								+ " has been added to the download queue");
						downloadManager.addNewDownload(downloadUrl, filename,
								storePath + filename, true, false, null);
						// downloadManager.addNewDownload(
						// downloadUrl, filename,
						// GalleryPath + filename, true,
						// true, null);
					}
				} catch (DbException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
		return convertView;
	}

	class Item {
		TextView fileName;
		Button downloadBtn;
	}

	private String getFileName(String url) {
		String fileName = null;
		if (url != null) {
			fileName = url.split("\\/")[url.split("\\/").length - 1];
		}
		return fileName;

	}

}
