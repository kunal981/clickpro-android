package com.clickpro.adapter;

import java.util.List;

import com.clickpro.ccu.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

public class SettingDetailAdapter extends BaseAdapter {
	private Context context;
	private List<String> lists;
	private int index = 0;

	public SettingDetailAdapter(Context context, List<String> lists) {
		// TODO Auto-generated constructor stub
		this.lists = lists;
		this.context = context;

	}

	public void setSelectedPisition(int index) {
		this.index = index;
	}

	public int getSelectedPisition() {
		return index;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		int size = 0;
		if (lists != null) {
			if (lists.size() > 0) {
				size = lists.size();
			} else {
				size = 0;
			}
		}
		return size;
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return lists.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(int arg0, View convertView, ViewGroup arg2) {
		// TODO Auto-generated method stub
		Item item = null;
		if (convertView == null) {
			item = new Item();
			convertView = LayoutInflater.from(context).inflate(
					R.layout.setting_detail_item, null);
			item.itemName = (TextView) convertView.findViewById(R.id.itemName);
			item.radioBtn = (RadioButton) convertView
					.findViewById(R.id.radioBtn);
			convertView.setTag(item);
		} else {
			item = (Item) convertView.getTag();
		}
		item.itemName.setText(lists.get(arg0));
		if (index == arg0) {
			item.radioBtn.setChecked(true);
		} else {
			item.radioBtn.setChecked(false);
		}

		return convertView;
	}

	public class Item {
		TextView itemName;
		public RadioButton radioBtn;
	}
}
