/**
 * 
 */
package com.clickpro.entity;

import java.io.Serializable;

/**
 * @description
 * @version 1.0
 * @author ������
 * @update 2013-11-3 ����4:51:57
 */

public class BaseReturns implements Serializable {
	private static final long serialVersionUID = 1L;
	private int rval;
	private int param_size;

	public int getRval() {
		return rval;
	}

	public void setRval(int rval) {
		this.rval = rval;
	}

	public int getParam_size() {
		return param_size;
	}

	public void setParam_size(int param_size) {
		this.param_size = param_size;
	}

}
