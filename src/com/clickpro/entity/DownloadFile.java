/**
 * 
 */
package com.clickpro.entity;

import java.io.Serializable;

import android.app.Notification;

/**
 * @description
 * @version 1.0
 * @author ������
 * @update 2013-11-26 ����10:12:08
 */

public class DownloadFile implements Serializable {
	private static final long serialVersionUID = 1L;
	public String fileName = "";
	public Notification notification;
	public long downloadSize;
	public long fileSize;
	public String downloadStatus = "";
	public int notificationId;
	public String fileAbsolutionPath;

}
