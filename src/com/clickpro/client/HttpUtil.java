/**
 * 
 */
package com.clickpro.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

/**
 * @description
 * @version 1.0
 * @author ������
 * @update 2013-11-6 ����6:32:03
 */

public class HttpUtil {
	public static int getResponse(String path) {
		int httpCode = 0;
		/** ���ؽ�� **/
		String responseBody = "";
		HttpClient httpClient = new DefaultHttpClient();
		HttpParams httphParams = httpClient.getParams();
		HttpGet httpGet = new HttpGet(path);
		HttpConnectionParams.setConnectionTimeout(httphParams, 10 * 1000);
		HttpConnectionParams.setSoTimeout(httphParams, 15 * 1000);
		try {
			HttpResponse response = httpClient.execute(httpGet);
			httpCode = response.getStatusLine().getStatusCode();
			if (httpCode == HttpURLConnection.HTTP_OK && response != null) {
				responseBody = EntityUtils.toString(response.getEntity(),
						HTTP.UTF_8);

			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return httpCode;

	}

	public static String getResponseStr(String path) {
		int httpCode = 0;
		/** ���ؽ�� **/
		String responseBody = "";
		InputStream inputStream = null;
		HttpClient httpClient = new DefaultHttpClient();
		HttpParams httphParams = httpClient.getParams();
		HttpGet httpGet = new HttpGet(path);
		HttpConnectionParams.setConnectionTimeout(httphParams, 10 * 1000);
		HttpConnectionParams.setSoTimeout(httphParams, 15 * 1000);
		try {
			HttpResponse response = httpClient.execute(httpGet);
			httpCode = response.getStatusLine().getStatusCode();
			if (httpCode == HttpURLConnection.HTTP_OK && response != null) {
				responseBody = EntityUtils.toString(response.getEntity(),
						HTTP.UTF_8);

			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return responseBody;

	}

}
