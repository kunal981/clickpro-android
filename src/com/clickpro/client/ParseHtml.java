/**
 * 
 */
package com.clickpro.client;

import java.util.ArrayList;
import java.util.HashMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.clickpro.util.Global;

/**
 * @description
 * @version 1.0
 * @author ������
 * @update 2013-11-20 ����9:34:54
 */

public class ParseHtml {

	public static HashMap<Integer, ArrayList<String>> parseSet() {
		HashMap<Integer, ArrayList<String>> hm = new HashMap<Integer, ArrayList<String>>();
		Document doc;
		String res = HttpUtil.getResponseStr(Global.getVideoSetValue);
		doc = Jsoup.parse(res);
		Elements selects = doc.getElementsByTag("select");
		if (selects.size() > 0) {
			// ����html �õ�video resulation ��ֵ
			ArrayList<String> vr = new ArrayList<String>();
			Element select = selects.get(0);
			Elements options = select.getElementsByTag("option");
			if (options.size() > 0) {
				vr.add(options.get(0).text());
				for (Element option : options) {
					if (vr.size() > 0
							&& !vr.get(0).equalsIgnoreCase(option.text())) {
						vr.add(option.text());
					}

				}

			}
			hm.put(0, vr);

			// ����html �õ�dual stream ��ֵ
			ArrayList<String> ds = new ArrayList<String>();
			select = selects.get(2);
			options = select.getElementsByTag("option");
			if (options.size() > 0) {
				ds.add(options.get(0).text());
				for (Element option : options) {
					if (ds.size() > 0
							&& !ds.get(0).equalsIgnoreCase(option.text())) {
						ds.add(option.text());
					}

				}

			}
			hm.put(2, ds);

			// ����html �õ�video stamp ��ֵ
			ArrayList<String> vs = new ArrayList<String>();
			select = selects.get(5);
			options = select.getElementsByTag("option");
			if (options.size() > 0) {
				vs.add(options.get(0).text());
				for (Element option : options) {
					if (vs.size() > 0
							&& !vs.get(0).equalsIgnoreCase(option.text())) {
						vs.add(option.text());
					}

				}

			}
			hm.put(3, vs);

		}
		res = HttpUtil.getResponseStr(Global.getPhotoSetValue);
		doc = Jsoup.parse(res);
		selects = doc.getElementsByTag("select");
		if (selects.size() > 0) {
			// ����html �õ�photo resulation ��ֵ
			ArrayList<String> pr = new ArrayList<String>();
			Element select = selects.get(1);
			Elements options = select.getElementsByTag("option");
			if (options.size() > 0) {
				pr.add(options.get(0).text());
				for (Element option : options) {
					if (pr.size() > 0
							&& !pr.get(0).equalsIgnoreCase(option.text())
							&& !option.text().equals("")) {
						pr.add(option.text());
					}

				}

			}
			hm.put(1, pr);
			// ����html �õ�photo stamp ��ֵ
			ArrayList<String> ps = new ArrayList<String>();
			select = selects.get(4);
			options = select.getElementsByTag("option");
			if (options.size() > 0) {
				ps.add(options.get(0).text());
				for (Element option : options) {
					if (ps.size() > 0
							&& !ps.get(0).equalsIgnoreCase(option.text())) {
						ps.add(option.text());
					}

				}

			}

			hm.put(4, ps);

			// ����html �õ�burst mode ��ֵ
			ArrayList<String> bm = new ArrayList<String>();
			select = selects.get(0);
			options = select.getElementsByTag("option");
			if (options.size() > 0) {
				bm.add(options.get(0).text());
				for (Element option : options) {
					if (bm.size() > 0
							&& !bm.get(0).equalsIgnoreCase(option.text())) {
						bm.add(option.text());
					}

				}
			}

			hm.put(5, bm);

		}

		return hm;

	}

	/**
	 * ��ȡ��ѡ�е�ֵ
	 * 
	 * @return
	 * @description ��һ�仰˵�����������ʲô
	 * @version 1.0
	 * @author ������
	 * @update 2013-12-5 ����11:06:19
	 */
	public static HashMap<Integer, ArrayList<String>> parseSet2() {
		HashMap<Integer, ArrayList<String>> hm = new HashMap<Integer, ArrayList<String>>();
		Document doc;
		String res = HttpUtil.getResponseStr(Global.getVideoSetValue);
		doc = Jsoup.parse(res);
		Elements selects = doc.getElementsByTag("select");
		if (selects.size() > 0) {
			// ����html �õ�video resulation ��ֵ
			ArrayList<String> vr = new ArrayList<String>();
			Element select = selects.get(0);
			Elements options = select.getElementsByTag("option");
			if (options.size() > 0) {
				vr.add(options.get(0).text());
			}
			hm.put(0, vr);

			// ����html �õ�dual stream ��ֵ
			ArrayList<String> ds = new ArrayList<String>();
			select = selects.get(2);
			options = select.getElementsByTag("option");
			if (options.size() > 0) {
				ds.add(options.get(0).text());
			}
			hm.put(2, ds);

			// ����html �õ�video stamp ��ֵ
			ArrayList<String> vs = new ArrayList<String>();
			select = selects.get(5);
			options = select.getElementsByTag("option");
			if (options.size() > 0) {
				vs.add(options.get(0).text());
			}
			hm.put(3, vs);

		}
		res = HttpUtil.getResponseStr(Global.getPhotoSetValue);
		doc = Jsoup.parse(res);
		selects = doc.getElementsByTag("select");
		if (selects.size() > 0) {
			// ����html �õ�photo resulation ��ֵ
			ArrayList<String> pr = new ArrayList<String>();
			Element select = selects.get(1);
			Elements options = select.getElementsByTag("option");
			if (options.size() > 0) {
				pr.add(options.get(0).text());
			}
			hm.put(1, pr);
			// ����html �õ�photo stamp ��ֵ
			ArrayList<String> ps = new ArrayList<String>();
			select = selects.get(4);
			options = select.getElementsByTag("option");
			if (options.size() > 0) {
				ps.add(options.get(0).text());
			}

			hm.put(4, ps);

		}

		return hm;

	}

	/***
	 * ��ȡvideo Resulation
	 * 
	 * @return
	 * @description ��һ�仰˵�����������ʲô
	 * @version 1.0
	 * @author ������
	 * @update 2013-11-22 ����8:41:52
	 */
	public static String getVideoResulation() {
		String videoResulation = "";
		Document doc;
		String res = HttpUtil.getResponseStr(Global.getVideoSetValue);
		doc = Jsoup.parse(res);
		Elements selects = doc.getElementsByTag("select");
		if (selects.size() > 0) {
			// ����html �õ�video resulation ��ֵ
			ArrayList<String> vr = new ArrayList<String>();
			Element select = selects.get(0);
			Elements options = select.getElementsByTag("option");
			if (options.size() > 0) {
				videoResulation = options.get(0).text();
			}
		}
		return videoResulation;
	}

	/***
	 * ��ȡphoto Resulation
	 * 
	 * @return
	 * @description ��һ�仰˵�����������ʲô
	 * @version 1.0
	 * @author ������
	 * @update 2013-11-22 ����8:41:52
	 */
	public static String getPhotoResulation() {
		String photoResulation = "";
		String res = HttpUtil.getResponseStr(Global.getPhotoSetValue);
		Document doc = Jsoup.parse(res);
		Elements selects = doc.getElementsByTag("select");
		if (selects.size() > 0) {
			// ����html �õ�photo resulation ��ֵ
			ArrayList<String> pr = new ArrayList<String>();
			Element select = selects.get(1);
			Elements options = select.getElementsByTag("option");
			if (options.size() > 0) {
				photoResulation = options.get(0).text();
			}

		}
		return photoResulation;
	}
}
