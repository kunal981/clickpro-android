/**
 * 
 */
package com.clickpro.client;

import java.io.IOException;
import java.util.Date;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.clickpro.util.Global;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;

/**
 * ������ͷ���ȡͼƬ,��ʾ����Ƶ
 * 
 * @description
 * @version 1.0
 * @author ������
 * @update 2013-11-6 ����10:38:14
 */

public class GetImageThread extends Thread {
	public static boolean isRun = true;
	Handler mHandler;

	public GetImageThread(Handler mHandler) {
		// TODO Auto-generated constructor stub
		this.mHandler = mHandler;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet;
			HttpResponse localHttpResponse;
			while (true) {
				if (isRun) {
					String url = "http://192.168.42.1/mjpeg/amba.jpg?ts="
							+ new Date().getTime();
					httpGet = new HttpGet(url);

					localHttpResponse = httpClient.execute(httpGet);
					if (localHttpResponse.getStatusLine().getStatusCode() == 200) {
						Global.isConn = true;
						Bitmap localBitmap = BitmapFactory
								.decodeStream(localHttpResponse.getEntity()
										.getContent());
						if (localBitmap != null) {
							mHandler.obtainMessage(0, localBitmap)
									.sendToTarget();
						}

					} else {
						Global.isConn = false;
					}
				}

			}

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			Global.isConn = false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Global.isConn = false;
		}

	}
}
