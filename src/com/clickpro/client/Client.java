package com.clickpro.client;

//package com.gumeipost.client;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.net.InetSocketAddress;
//import java.net.Socket;
//
//import com.gumeipost.util.Global;
//
//import android.content.Context;
//import android.content.Intent;
//import android.os.Handler;
//
///**
// * 客户端
// */
//public class Client {
//
//	private Socket socket;
//	/** 获取图片的线程 **/
//	private GetImageThread imageThread;
//	private String ip;
//	private int port;
//	/** 是否连接成功 **/
//	private boolean isConn;
//	private int timeout = 3000;
//	private InputStream is;
//	private Handler mHandler;
//    private Context mContext;
//	public Client(String ip, int port,Context mContext) {
//		this.ip = ip;
//		this.port = port;
//		this.mContext=mContext;
//	}
//
//	public void setHandler(Handler mHandler) {
//		this.mHandler = mHandler;
//	}
//
//	public boolean connectionServer() {
//		try {
//			socket = new Socket();
//			socket.connect(new InetSocketAddress(ip, port), timeout);
//			if (socket.isConnected()) {
//				isConn = true;
//				is = socket.getInputStream();
//				//获取预览画面 先注释掉
////				imageThread = new GetImageThread(mHandler);
////				imageThread.start();
//
//				new Thread(new GetMessage()).start();
//			}
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			isConn = false;
//			e.printStackTrace();
//		}
//		return isConn;
//	}
//	
//
//	public Socket getSocket() {
//		return socket;
//	}
//
//	public void setSocket(Socket socket) {
//		this.socket = socket;
//	}
//	
//	
//    public boolean isConn() {
//		return isConn;
//	}
//
//	public void setConn(boolean isConn) {
//		this.isConn = isConn;
//	}
//
//	/**关闭连接**/
//	public void disConn() {
//		if (socket != null) {
//			try {
//				socket.close();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//		if (is != null) {
//			try {
//				is.close();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//
//	}
//
//	/**
//	 * 从服务端返回的信息
//	 * 
//	 * @description
//	 * @version 1.0
//	 * @author 梁卫西
//	 * @update 2013-11-8 下午2:43:32
//	 */
//	class GetMessage implements Runnable {
//
//		@Override
//		public void run() {
//			// TODO Auto-generated method stub
//			if (isConn) {
//				byte[] buffer = new byte[1024];
//				int bytes;
//				try {
//					while (true) {
//						bytes = is.read(buffer);
//						String result = new String(buffer, 0, bytes);
//						Intent intent=new Intent("feedback");
//						intent.putExtra("tag", Global.tag);
//						intent.putExtra("result", result);
//						mContext.sendBroadcast(intent);
//						System.out.println("ccu===" + result);
//					}
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//
//		}
//
//	}
//
// }
