/**
 * 
 */
package com.clickpro.client;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.clickpro.ccu.MainActivity;
import com.clickpro.ccu.R;
import com.clickpro.entity.DownloadFile;
import com.clickpro.util.CommonUtil;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.RemoteViews;

/**
 * ������Ƶ
 * 
 * @description
 * @version 1.0
 * @author ������
 * @update 2013-11-25 ����4:43:58
 */

public class DownloadService extends Service {
	private NotificationManager notificationManager;
	private Notification notification;
	private String url;
	private String storePath = Environment.getExternalStorageDirectory()
			+ "/RD990/";
	private static final int DOWNLOADING = 1;
	private static final int DOWNLOADFAIL = 2;
	private static final int DOWNLOADSUCCESS = 3;
	boolean D = false;
	private final String TAG = "DS";
	private RemoteViews remoteViews = null;
	private HashMap<String, String> hm = new HashMap<String, String>();

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub

		url = intent.getStringExtra("url");
		String fileName = getFileName(url);
		if (hm.containsKey(fileName)) {
		} else {
			hm.put(fileName, url);
			int notificationId = (int) (Math.random() * 10000);
			createNotification(fileName, notificationId);
		}

		return START_REDELIVER_INTENT;

	}

	Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			DownloadFile downloadFile = (DownloadFile) msg.obj;
			Notification nf;
			switch (msg.what) {
			case DOWNLOADING:
				if (D) {
					Log.e(TAG, "downloading...");
				}

				remoteViews.setTextViewText(R.id.fileName,
						downloadFile.fileName);
				long downloadSize = downloadFile.downloadSize;
				long fileSize = downloadFile.fileSize;
				int progress = (int) (downloadSize * 100 / fileSize);
				remoteViews.setTextViewText(R.id.percent, progress + "%");
				remoteViews.setProgressBar(R.id.pb, 100, progress, false);
				remoteViews.setTextViewText(R.id.fileAbsolutePath,
						downloadFile.fileAbsolutionPath);

				showNotification(downloadFile.notificationId);

				break;
			case DOWNLOADSUCCESS:
				if (D) {
					Log.e(TAG, "download success...");
				}

				remoteViews.setTextViewText(R.id.fileName,
						downloadFile.fileName);
				remoteViews.setTextViewText(R.id.percent,
						downloadFile.downloadStatus);
				remoteViews.setTextViewText(R.id.fileAbsolutePath,
						downloadFile.fileAbsolutionPath);

				showNotification(downloadFile.notificationId);

				break;
			case DOWNLOADFAIL:
				if (D) {
					Log.e(TAG, "download fail...");
				}
				remoteViews.setTextViewText(R.id.fileName,
						downloadFile.fileName);
				remoteViews.setTextViewText(R.id.percent,
						downloadFile.downloadStatus);
				remoteViews.setTextViewText(R.id.fileAbsolutePath,
						downloadFile.fileAbsolutionPath);

				showNotification(downloadFile.notificationId);
				break;
			default:
				break;
			}

		};
	};

	/***
	 * �����߳�
	 * 
	 * @description
	 * @version 1.0
	 * @author ������
	 * @update 2013-11-25 ����4:48:26
	 */
	class DownloadRunnable implements Runnable {
		String downloadUrl;
		FileOutputStream fos;
		InputStream is;
		Notification nf;
		int notificationId;
		String fileName;

		public DownloadRunnable(String downloadUrl, Notification nf,
				int notificationId, String fileName) {
			// TODO Auto-generated constructor stub
			this.downloadUrl = downloadUrl;
			this.nf = nf;
			this.notificationId = notificationId;
			this.fileName = fileName;
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			try {
				DefaultHttpClient client = new DefaultHttpClient();
				HttpGet httpGet = new HttpGet(downloadUrl);
				HttpResponse response = client.execute(httpGet);
				int count = 0;
				if (response.getStatusLine().getStatusCode() == 200) {
					long fileSize = response.getEntity().getContentLength();
					is = response.getEntity().getContent();
					long downloadSize = 0;
					int len;
					byte[] buffer = new byte[1024];

					File file = new File(storePath);
					if (!file.exists()) {
						file.mkdirs();
					}
					fos = new FileOutputStream(storePath + fileName);
					while ((len = is.read(buffer)) != -1) {
						fos.write(buffer, 0, len);
						downloadSize += len;
						// ������
						if (D) {
							Log.e(TAG, "������...");
						}
						if (count == 512 || downloadSize == fileSize) {
							DownloadFile downloadFile = new DownloadFile();
							downloadFile.downloadSize = downloadSize;
							downloadFile.fileSize = fileSize;
							downloadFile.fileName = fileName;
							downloadFile.notification = nf;
							downloadFile.notificationId = notificationId;
							downloadFile.fileAbsolutionPath = storePath
									+ fileName;
							mHandler.obtainMessage(DOWNLOADING, downloadFile)
									.sendToTarget();
							count = 0;
						}

						if (downloadSize == fileSize) {
							// �������
							if (D) {
								Log.e(TAG, "�������...");
							}
							hm.remove(fileName);
							DownloadFile downloadFile = new DownloadFile();
							downloadFile.notification = nf;
							downloadFile.fileName = fileName;
							downloadFile.notificationId = notificationId;
							downloadFile.fileAbsolutionPath = storePath
									+ fileName;
							downloadFile.downloadStatus = getString(R.string.download_success);
							mHandler.obtainMessage(DOWNLOADSUCCESS,
									downloadFile).sendToTarget();
						}
						count++;

					}

				} else {
					// ����ʧ��
					if (D) {
						Log.e(TAG, "����ʧ��...");
					}
					hm.remove(fileName);
					DownloadFile downloadFile = new DownloadFile();
					downloadFile.notification = nf;
					downloadFile.fileName = fileName;
					downloadFile.notificationId = notificationId;
					downloadFile.downloadStatus = getString(R.string.download_failed);
					downloadFile.fileAbsolutionPath = storePath + fileName;
					mHandler.obtainMessage(DOWNLOADFAIL, downloadFile)
							.sendToTarget();
				}
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				hm.remove(fileName);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				hm.remove(fileName);
			} finally {
				try {
					if (fos != null) {
						fos.close();
					}
					if (is != null) {
						is.close();
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

			}

		}
	}

	/**
	 * ����֪ͨ
	 * 
	 * @version 1.0
	 * @author ������
	 * @update 2013-11-25 ����4:49:09
	 */
	private void createNotification(String fileName, int notificationId) {
		notification = new Notification(R.drawable.download, "",
				System.currentTimeMillis());
		remoteViews = new RemoteViews(getPackageName(), R.layout.notification);
		remoteViews.setImageViewResource(R.id.tubiao, R.drawable.download);
		remoteViews.setTextViewText(R.id.fileName, fileName);
		remoteViews.setTextViewText(R.id.percent, "0%");
		remoteViews.setProgressBar(R.id.pb, 100, 0, false);
		remoteViews
				.setTextViewText(R.id.fileAbsolutePath, storePath + fileName);
		notification.contentView = remoteViews;
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
				new Intent(this, MainActivity.class), 0);
		notification.contentIntent = contentIntent;
		showNotification(notificationId);
		new Thread(new DownloadRunnable(url, notification, notificationId,
				fileName)).start();
		notificationId = notificationId + 1;

	}

	private void showNotification(int notificationId) {
		notificationManager.notify(notificationId, notification);
	}

	private String getFileName(String url) {
		int start = url.lastIndexOf("/") + 1;
		int end = url.length();
		String fileName = url.substring(start, end).toString();
		return fileName;
	}

}
