package com.clickpro.ccu;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.tsz.afinal.FinalBitmap;

import com.clickpro.photoview.HackyViewPager;
import com.clickpro.photoview.PhotoView;
import com.clickpro.util.CommonUtil;
import com.clickpro.util.SaveImages;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;

/**
 * �ط�չ������
 * 
 * @description
 * @version 1.0
 * @update 2013-10-10 ����7:49:08
 */
public class PlayBackDetailActivity extends FragmentActivity implements
		OnClickListener {
	private RelativeLayout backLinear;
	private HackyViewPager viewpager;
	private ImageAdapter imageAdapter;
	private int index;
	private List<String> temp = new ArrayList<String>();
	private Context context;
	private ImageView previous, next, download, delete;
	private TextView photoName;
	private String pName = "";
	private RelativeLayout top;
	private LinearLayout bottom;
	private MyBroadcastReceiver myBroadcastReveiver;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.play_back_detail);
		context = this;
		if (getIntent() != null) {
			temp = getIntent().getStringArrayListExtra("list");
			index = getIntent().getIntExtra("index", 0);
			pName = getIntent().getStringExtra("photoName");
		}
		initView();
		register();
	}

	private void initView() {
		// TODO Auto-generated method stub
		backLinear = (RelativeLayout) findViewById(R.id.backLinear);
		backLinear.setOnClickListener(this);
		viewpager = (HackyViewPager) findViewById(R.id.viewpager);
		imageAdapter = new ImageAdapter(getSupportFragmentManager());
		imageAdapter.setData(temp);
		viewpager.setAdapter(imageAdapter);
		viewpager.setCurrentItem(index);
		viewpager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub
				index = arg0;
				photoName.setText(getPhotoName(temp.get(index)));
				viewpager.setCurrentItem(index);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}
		});

		photoName = (TextView) findViewById(R.id.photoName);
		if (pName != null) {
			photoName.setText(getPhotoName(pName));
		}
		previous = (ImageView) findViewById(R.id.previous);
		previous.setOnClickListener(this);
		next = (ImageView) findViewById(R.id.next);
		next.setOnClickListener(this);
		download = (ImageView) findViewById(R.id.download);
		download.setOnClickListener(this);
		delete = (ImageView) findViewById(R.id.delete);
		delete.setOnClickListener(this);
		top = (RelativeLayout) findViewById(R.id.top);
		bottom = (LinearLayout) findViewById(R.id.bottom);

	}

	/**
	 * ͼƬ������
	 */
	public class ImageAdapter extends FragmentPagerAdapter {
		private List<String> list;

		public ImageAdapter(FragmentManager fm) {
			super(fm);
		}

		public void setData(List<String> list) {
			this.list = list;
		}

		@Override
		public int getCount() {
			return list.size();
		}

		@Override
		public Fragment getItem(int position) {
			ImageFragment imageFragment = new ImageFragment();
			return imageFragment.newInstance(list, position);
		}
	}

	@SuppressLint("ValidFragment")
	public class ImageFragment extends Fragment {
		int num;
		private List<String> photoNames;

		public ImageFragment() {
			super();
		}

		/**
		 * Create a new instance of CountingFragment, providing "num" as an
		 * argument.
		 */
		public ImageFragment newInstance(List<String> list, int num) {
			ImageFragment f = new ImageFragment();
			Bundle args = new Bundle();
			args.putInt("num", num);
			args.putSerializable("flag", (Serializable) list);
			f.setArguments(args);

			return f;
		}

		/**
		 * When creating, retrieve this instance's number from its arguments.
		 */
		@SuppressWarnings("unchecked")
		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			num = getArguments() != null ? getArguments().getInt("num") : 0;
			photoNames = (List<String>) getArguments().getSerializable("flag");
		}

		/**
		 * The Fragment's UI is just a simple text view showing its instance
		 * number.
		 */
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {

			final ItemView item = new ItemView();
			View view = inflater.inflate(R.layout.playback_detail_item, null);
			item.imageItem = (PhotoView) view.findViewById(R.id.imageview);
			view.setTag(item);
			FinalBitmap.create(context).display(item.imageItem, temp.get(num));

			return view;

		}

		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			super.onActivityCreated(savedInstanceState);
		}

		class ItemView {
			PhotoView imageItem;
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.backLinear:
			finish();
			overridePendingTransition(0, 0);
			break;
		case R.id.previous:
			if (index != 0) {
				index--;
				viewpager.setCurrentItem(index);
				photoName.setText(getPhotoName(temp.get(index)));
			}
			break;
		case R.id.next:
			if (index != temp.size() - 1) {
				index++;
				viewpager.setCurrentItem(index);
				photoName.setText(getPhotoName(temp.get(index)));
			}
			break;
		case R.id.download:
			Bitmap bitmap = FinalBitmap.create(context).getBitmapFromCache(
					temp.get(index));
			// ����ͼƬ�����
			if (bitmap != null) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
				String now = sdf.format(new Date());
				String fileName = "IMAGE_" + now;
				SaveImages.storeInSD(bitmap, fileName, mHandler);
			}

			break;
		case R.id.delete:
			break;
		}
	}

	/***
	 * ��ȡ��Ƭ����
	 * 
	 * @param url
	 * @return
	 * @description ��һ�仰˵�����������ʲô
	 * @version 1.0
	 * @author ������
	 * @update 2013-11-12 ����4:01:12
	 */
	private String getPhotoName(String url) {
		String name = "";
		if (url != null) {
			name = url.split("\\/")[url.split("\\/").length - 1];
		}
		return name;
	}

	/**
	 * ��ʾ�����ض����͵ײ�
	 * 
	 * @version 1.0
	 * @author ������
	 * @update 2013-11-27 ����4:33:24
	 */
	private void showOrHide() {
		if (top.getVisibility() == View.INVISIBLE) {
			top.setVisibility(View.VISIBLE);
			bottom.setVisibility(View.INVISIBLE);
		} else {
			top.setVisibility(View.INVISIBLE);
			bottom.setVisibility(View.INVISIBLE);
		}

	}

	private void register() {
		myBroadcastReveiver = new MyBroadcastReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction("showHide");
		registerReceiver(myBroadcastReveiver, filter);
	}

	class MyBroadcastReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			if (intent.getAction().equals("showHide")) {
				showOrHide();
			}
		}

	}

	Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 0:
				boolean isSuccess = (Boolean) msg.obj;
				if (isSuccess) {
					CommonUtil.showToast(context,
							getString(R.string.store_success));
				} else {
					CommonUtil.showToast(context,
							getString(R.string.store_fail));
				}
				break;

			default:
				break;
			}
		};
	};

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(myBroadcastReveiver);
	}

}
