package com.clickpro.ccu;

import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.Toast;

public class ChoiceActivity extends BaseActivity {
	private ImageView wifi, replay, help;
	private Context mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.choice);
		mContext = this;
		wifi = (ImageView) findViewById(R.id.wifi);
		wifi.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, MainActivity.class);
				startActivity(intent);
			}
		});
		replay = (ImageView) findViewById(R.id.replay);
		replay.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, DownloadListActivity.class);
				startActivity(intent);
			}
		});
		help = (ImageView) findViewById(R.id.help);
		help.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, SplashActivity.class);
				startActivity(intent);
			}
		});
	}

	/**
	 * �˵������ؼ���Ӧ
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			exitBy2Click(); // ����˫���˳�����
		}
		return false;
	}

	/**
	 * ˫���˳�����
	 */
	private static Boolean isExit = false;
	private Timer tExit;

	private void exitBy2Click() {
		tExit = null;
		if (isExit == false) {
			isExit = true; // ׼���˳�
			Toast.makeText(mContext, getString(R.string.exit),
					Toast.LENGTH_SHORT).show();
			tExit = new Timer();
			tExit.schedule(new TimerTask() {
				@Override
				public void run() {
					isExit = false; // ȡ���˳�
				}
			}, 2000); // ���2������û�а��·��ؼ�����������ʱ��ȡ�����ղ�ִ�е�����

		} else {
			// �˳�
			AppManager.getAppManager().AppExit(mContext);
		}
	}
}
