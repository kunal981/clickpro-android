package com.clickpro.ccu;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jsoup.helper.StringUtil;

import net.tsz.afinal.FinalBitmap;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInstaller.Session;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.clickpro.adapter.SettingAdapter;
import com.clickpro.client.HttpUtil;
import com.clickpro.client.ParseHtml;
import com.clickpro.entity.Property;
import com.clickpro.util.CommonUtil;
import com.clickpro.util.Global;
import com.clickpro.view.CustomDialog;
import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.android.AuthActivity;
import com.dropbox.client2.session.AccessTokenPair;
import com.dropbox.client2.session.AppKeyPair;

/**
 * ���ý���
 * 
 * @description
 * @version 1.0
 * @update 2013-10-10 ����7:49:08
 */
public class SettingActivity extends BaseActivity implements OnClickListener {
	private ListView listview;
	TextView t;
	private SettingAdapter adapter;
	private List<Property> lists = new ArrayList<Property>();
	private Context mContext;
	private LinearLayout back;
	private HashMap<Integer, ArrayList<String>> hm = new HashMap<Integer, ArrayList<String>>();
	private ArrayList<String> resolution;
	private MyBroadcastReceiver mBroadcastReceiver;
	private Button refresh;
	// �����������
	private int clickedItem;
	// for Drop Box Authentication
	private static final String APP_KEY = "vms1kkpy97ducky";
	private static final String APP_SECRET = "878suyz710yqxtn";

	private static final String ACCOUNT_PREFS_NAME = "prefs";
	private static final String ACCESS_KEY_NAME = "ACCESS_KEY";
	private static final String ACCESS_SECRET_NAME = "ACCESS_SECRET";

	private static final boolean USE_OAUTH1 = false;
	AndroidAuthSession session;
	DropboxAPI<AndroidAuthSession> mApi;

	private boolean mLoggedIn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		session = buildSession();
		Log.e("session", "" + session.toString());
		mApi = new DropboxAPI<AndroidAuthSession>(session);
		Log.e("mApi", "" + mApi);
		checkAppKeySetup();
		setContentView(R.layout.setting);
		mContext = this;
		initView();
		register();
		if (Global.hashmap != null) {
			hm.clear();
			hm.putAll(Global.hashmap);
		}
	}

	private void initView() {
		// TODO Auto-generated method stub
		back = (LinearLayout) findViewById(R.id.back);
		back.setOnClickListener(this);
		refresh = (Button) findViewById(R.id.refresh);
		refresh.setOnClickListener(this);
		t = (TextView) findViewById(R.id.click);

		refresh.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new Request().execute();
			}
		});
		listview = (ListView) findViewById(R.id.listview);
		lists = addData();
		adapter = new SettingAdapter(mContext, lists);
		listview.setAdapter(adapter);
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				clickedItem = arg2;
				Property property = (Property) arg0.getItemAtPosition(arg2);
				if (property != null) {
					if (arg2 == 2) {
						String videoStamp = lists.get(3).getValue();
						if (!videoStamp.equals("off")) {
							showDialog(getString(R.string.set_video_stamp));
						} else {
							resolution = hm.get(2);
							Intent intent = new Intent(mContext,
									SettingDetailActivity.class);
							intent.putStringArrayListExtra("list", resolution);
							intent.putExtra("value", property.getValue());
							intent.putExtra("title", property.getName());
							intent.putExtra("position", arg2);
							startActivity(intent);
						}
					} else if (arg2 == 3) {
						String dualStreams = lists.get(2).getValue();
						if (dualStreams.equals("on")) {
							showDialog(getString(R.string.set_dual_streams));
						} else {
							resolution = hm.get(3);
							Intent intent = new Intent(mContext,
									SettingDetailActivity.class);
							intent.putStringArrayListExtra("list", resolution);
							intent.putExtra("value", property.getValue());
							intent.putExtra("title", property.getName());
							intent.putExtra("position", arg2);
							startActivity(intent);
						}

					} else if (arg2 == 6) {
						showAlertDialog();
					} else if (arg2 == 7) {
						Intent intent = new Intent(mContext,
								SplashActivity.class);
						startActivity(intent);
					} else if (arg2 == 8) {

						isNetworkAvailable(h, 2000);

					} else {
						if (arg2 == 0) {
							// resolution = videoResolutionData();
							resolution = hm.get(0);
						} else if (arg3 == 1) {
							// resolution = photoResolutionData();
							resolution = hm.get(1);
						} else if (arg3 == 4) {
							// resolution = photoStampData();
							resolution = hm.get(4);
						}

						else if (arg3 == 5) {
							resolution = hm.get(5);
						}

						Intent intent = new Intent(mContext,
								SettingDetailActivity.class);
						intent.putStringArrayListExtra("list", resolution);
						intent.putExtra("value", property.getValue());
						intent.putExtra("title", property.getName());
						intent.putExtra("position", arg2);
						startActivity(intent);
					}

				}

			}
		});
	}

	private void showAlertDialog() {
		AlertDialog.Builder buidler = new AlertDialog.Builder(mContext);
		buidler.setMessage(getString(R.string.format_sd_ts));
		buidler.setPositiveButton(getString(R.string.cancel),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				});
		buidler.setNegativeButton(getString(R.string.sure),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						new FormatSDAsy(Global.formatsd).execute();
					}
				});
		buidler.show();
	}

	/***
	 * ������
	 * 
	 * @version 1.0
	 * @author ������
	 * @update 2013-11-22 ����11:33:22
	 */
	private void showDialog(String message) {
		AlertDialog.Builder buidler = new AlertDialog.Builder(mContext);
		buidler.setMessage(message);

		buidler.setNegativeButton(getString(R.string.sure),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				});
		buidler.show();
	}

	/**
	 * �������
	 * 
	 * @version 1.0
	 * @update 2013-10-11 ����12:05:02
	 */
	private List<Property> addData() {
		List<Property> temp = new ArrayList<Property>();
		Property pro = new Property();
		pro.setName("Video Resolution");
		temp.add(pro);

		pro = new Property();
		pro.setName("Photo Resolution");
		temp.add(pro);

		pro = new Property();
		pro.setName("Dual Streams");
		temp.add(pro);

		pro = new Property();
		pro.setName("Video Stamp");
		temp.add(pro);

		pro = new Property();
		pro.setName("Photo Stamp");
		temp.add(pro);

		pro = new Property();
		pro.setName("Burst Mode");
		temp.add(pro);

		pro = new Property();
		pro.setName("FORMAT");
		temp.add(pro);

		pro = new Property();
		pro.setName("Help");
		temp.add(pro);

		pro = new Property();
		pro.setName("Sync with Dropbox");
		temp.add(pro);

		return temp;

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.back:
			finish();
			break;
		case R.id.click:
			Log.e("", "Click");
			logOut();
			break;

		default:
			break;
		}
	}

	Handler h = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			if (msg.what != 1) { // code if not connected

				Log.e("Internet", "Noaccessibility");
				AlertDialog.Builder builder = new AlertDialog.Builder(
						SettingActivity.this);
				builder = new AlertDialog.Builder(SettingActivity.this);
				builder.setCancelable(false)
						.setMessage("Please Connect with Other Network")
						.setNegativeButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								});

				AlertDialog alertDialog = builder.create();
				alertDialog.show();

			} else { // code if connected
				Log.e("Internet", "Accessibility");
				if (mLoggedIn) {

					Intent intent = new Intent(getApplicationContext(),
							DropBoxStatus.class);
					startActivity(intent);
					// logOut();
				} else {

					// Start the remote authentication
					if (USE_OAUTH1) {
						mApi.getSession().startAuthentication(
								SettingActivity.this);
					} else {
						mApi.getSession().startOAuth2Authentication(
								SettingActivity.this);
					}
				}
			}
		}
	};

	// For Check Internet Access
	public static void isNetworkAvailable(final Handler handler,
			final int timeout) {
		// ask fo message '0' (not connected) or '1' (connected) on 'handler'
		// the answer must be send before before within the 'timeout' (in
		// milliseconds)

		new Thread() {
			private boolean responded = false;

			@Override
			public void run() {
				// set 'responded' to TRUE if is able to connect with google
				// mobile (responds fast)
				new Thread() {
					@Override
					public void run() {
						HttpGet requestForTest = new HttpGet(
								"http://m.google.com");
						try {
							new DefaultHttpClient().execute(requestForTest); // can
																				// last...
							responded = true;
						} catch (Exception e) {
						}
					}
				}.start();

				try {
					int waited = 0;
					while (!responded && (waited < timeout)) {
						sleep(100);
						if (!responded) {
							waited += 100;
						}
					}
				} catch (InterruptedException e) {
				} // do nothing
				finally {
					if (!responded) {
						handler.sendEmptyMessage(0);
					} else {
						handler.sendEmptyMessage(1);
					}
				}
			}
		}.start();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		AndroidAuthSession session = mApi.getSession();
		if (session.authenticationSuccessful()) {
			try {
				// Mandatory call to complete the auth
				session.finishAuthentication();

				// Store it locally in our app for later use
				storeAuth(session);
				setLoggedIn(true);
			} catch (IllegalStateException e) {
				showToast("Couldn't authenticate with Dropbox:"
						+ e.getLocalizedMessage());
				Log.i("Setting Activity", "Error authenticating", e);
			}

		}

		if (clickedItem == 5) {
			new Request().execute();
		} else {
			lists.get(0).setValue(Global.vResulation);
			lists.get(1).setValue(Global.photoResulation);
			lists.get(2).setValue(Global.dualStreams);
			lists.get(3).setValue(Global.videoStamp);
			lists.get(4).setValue(Global.photoStampValue);
			lists.get(5).setValue(Global.burstMode);

			adapter.notifyDataSetChanged();
		}

	}

	private void logOut() {
		// Remove credentials from the session
		mApi.getSession().unlink();

		// Clear our stored keys
		clearKeys();
		// Change UI state to display logged out version
		setLoggedIn(false);
	}

	/**
	 * Convenience function to change UI state based on being logged in
	 */
	private void setLoggedIn(boolean loggedIn) {
		mLoggedIn = loggedIn;
		if (loggedIn) {

		} else {

		}
	}

	private void checkAppKeySetup() {
		// Check to make sure that we have a valid app key
		// if (APP_KEY.startsWith("CHANGE") ||
		// APP_SECRET.startsWith("CHANGE")) {
		// showToast("You must apply for an app key and secret from developers.dropbox.com, and add them to the DBRoulette ap before trying it.");
		// finish();
		// return;
		// }

		// Check if the app has set up its manifest properly.
		Intent testIntent = new Intent(Intent.ACTION_VIEW);
		String scheme = "db-" + APP_KEY;
		String uri = scheme + "://" + AuthActivity.AUTH_VERSION + "/test";
		testIntent.setData(Uri.parse(uri));
		PackageManager pm = getPackageManager();
		if (0 == pm.queryIntentActivities(testIntent, 0).size()) {
			showToast("URL scheme in your app's "
					+ "manifest is not set up correctly. You should have a "
					+ "com.dropbox.client2.android.AuthActivity with the "
					+ "scheme: " + scheme);
			finish();
		}
	}

	private void showToast(String msg) {
		Toast error = Toast.makeText(this, msg, Toast.LENGTH_LONG);
		error.show();
	}

	/**
	 * Shows keeping the access keys returned from Trusted Authenticator in a
	 * local store, rather than storing user name & password, and
	 * re-authenticating each time (which is not to be done, ever).
	 */
	private void loadAuth(AndroidAuthSession session) {
		SharedPreferences prefs = getSharedPreferences(ACCOUNT_PREFS_NAME, 0);
		String key = prefs.getString(ACCESS_KEY_NAME, null);
		String secret = prefs.getString(ACCESS_SECRET_NAME, null);
		if (key == null || secret == null || key.length() == 0
				|| secret.length() == 0)
			return;

		if (key.equals("oauth2:")) {
			// If the key is set to "oauth2:", then we can assume the token is
			// for OAuth 2.
			session.setOAuth2AccessToken(secret);
		} else {
			// Still support using old OAuth 1 tokens.
			session.setAccessTokenPair(new AccessTokenPair(key, secret));
		}
	}

	/**
	 * Shows keeping the access keys returned from Trusted Authenticator in a
	 * local store, rather than storing user name & password, and
	 * re-authenticating each time (which is not to be done, ever).
	 */
	private void storeAuth(AndroidAuthSession session) {
		// Store the OAuth 2 access token, if there is one.
		String oauth2AccessToken = session.getOAuth2AccessToken();
		if (oauth2AccessToken != null) {
			SharedPreferences prefs = getSharedPreferences(ACCOUNT_PREFS_NAME,
					0);
			Editor edit = prefs.edit();
			edit.putString(ACCESS_KEY_NAME, "oauth2:");
			edit.putString(ACCESS_SECRET_NAME, oauth2AccessToken);
			edit.commit();
			return;
		}
		// Store the OAuth 1 access token, if there is one. This is only
		// necessary if
		// you're still using OAuth 1.
		AccessTokenPair oauth1AccessToken = session.getAccessTokenPair();
		if (oauth1AccessToken != null) {
			SharedPreferences prefs = getSharedPreferences(ACCOUNT_PREFS_NAME,
					0);
			Editor edit = prefs.edit();
			edit.putString(ACCESS_KEY_NAME, oauth1AccessToken.key);
			edit.putString(ACCESS_SECRET_NAME, oauth1AccessToken.secret);
			edit.commit();
			return;
		}
	}

	private void clearKeys() {
		SharedPreferences prefs = getSharedPreferences(ACCOUNT_PREFS_NAME, 0);
		Editor edit = prefs.edit();
		edit.clear();
		edit.commit();
	}

	private AndroidAuthSession buildSession() {
		AppKeyPair appKeyPair = new AppKeyPair(APP_KEY, APP_SECRET);

		AndroidAuthSession session = new AndroidAuthSession(appKeyPair);
		loadAuth(session);
		return session;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			finish();
		}
		return false;
	}

	/**
	 * @description
	 * @version 1.0
	 * @author ������
	 * @update 2013-11-8 ����1:39:41
	 */
	class FormatSDAsy extends AsyncTask<String, String, String> {
		String path;
		int resultCode;
		CustomDialog dialog;

		public FormatSDAsy(String path) {
			// TODO Auto-generated constructor stub
			this.path = path;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = new CustomDialog(mContext);
			dialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			resultCode = HttpUtil.getResponse(path);
			if (resultCode == 200) {
				// �����ǰͼƬ�Ļ���
				FinalBitmap.create(mContext).clearMemoryCache();
			}
			return null;

		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (dialog != null && dialog.isShowing()) {
				dialog.dismiss();
			}
			if (resultCode == 200) {
				CommonUtil.showCustomToast(mContext);
			} else {
				CommonUtil
						.showToast(mContext, getString(R.string.request_fail));
			}

		}
	}

	private void register() {
		mBroadcastReceiver = new MyBroadcastReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction("data");
		registerReceiver(mBroadcastReceiver, filter);
	}

	class MyBroadcastReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			if (intent.getAction().equals("data")) {
				if (Global.hashmap != null && Global.hashmap.size() > 0) {
					hm.clear();
					hm.putAll(Global.hashmap);
					for (int i = 0; i < Global.hashmap.size(); i++) {
						lists.get(i).setValue(Global.hashmap.get(i).get(0));
					}
					adapter.notifyDataSetChanged();

				}
			}
		}

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(mBroadcastReceiver);
	}

	/**
	 * ����
	 * 
	 * @description
	 * @version 1.0
	 * @author ������
	 * @update 2013-11-8 ����1:39:41
	 */
	class Request extends AsyncTask<String, String, String> {
		int resultCode;
		CustomDialog dialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = new CustomDialog(mContext);
			dialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			hm = ParseHtml.parseSet();
			return null;

		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (dialog != null && dialog.isShowing()) {
				dialog.dismiss();
			}
			if (hm != null && hm.size() > 0) {
				for (int i = 0; i < hm.size(); i++) {
					lists.get(i).setValue(hm.get(i).get(0));
				}
				adapter.notifyDataSetChanged();
				Global.vResulation = lists.get(0).getValue();
				Global.photoResulation = lists.get(1).getValue();
				Global.dualStreams = lists.get(2).getValue();
				Global.videoStamp = lists.get(3).getValue();
				Global.photoStampValue = lists.get(4).getValue();
				Global.hashmap.clear();
				Global.hashmap.putAll(hm);
			} else {
				CommonUtil.showToast(mContext,
						getString(R.string.connection_failed));
			}

		}

	}

}
