package com.clickpro.ccu;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInstaller.Session;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.clickpro.Fragment.Fragment_Synced_File;
import com.clickpro.Fragment.Fragmentsync;
import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.ProgressListener;
import com.dropbox.client2.DropboxAPI.UploadRequest;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;
import com.dropbox.client2.exception.DropboxFileSizeException;
import com.dropbox.client2.exception.DropboxIOException;
import com.dropbox.client2.exception.DropboxParseException;
import com.dropbox.client2.exception.DropboxPartialFileException;
import com.dropbox.client2.exception.DropboxServerException;
import com.dropbox.client2.exception.DropboxUnlinkedException;
import com.dropbox.client2.session.AccessTokenPair;
import com.dropbox.client2.session.AppKeyPair;

public class DropBox_File extends FragmentActivity implements OnClickListener {
	RelativeLayout mBack;
	LinearLayout mTab1, mTab2;
	TextView t1, t2;
	Fragmentsync mFragment_File_For_sync;
	Fragment_Synced_File mFragment_Synced_File;
	FragmentManager my_fragment_manager;
	FragmentTransaction fragmentTransaction;
	int selected_tab = 1;
	private Context mContext;
	Button upload;
	ArrayList<File> syncfile;
	// for Drop Box Authentication
	private static final String APP_KEY = "vms1kkpy97ducky";
	private static final String APP_SECRET = "878suyz710yqxtn";

	private static final String ACCOUNT_PREFS_NAME = "prefs";
	private static final String ACCESS_KEY_NAME = "ACCESS_KEY";
	private static final String ACCESS_SECRET_NAME = "ACCESS_SECRET";
	DropboxAPI<AndroidAuthSession> mApi;
	private final String PHOTO_DIR = "/TEST/";
	AndroidAuthSession session;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		mContext = this;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dropbox_list);
		session = buildSession();
		Log.e("session", "" + session.toString());
		mApi = new DropboxAPI<AndroidAuthSession>(session);
		mBack = (RelativeLayout) findViewById(R.id.backLinear1);
		mTab1 = (LinearLayout) findViewById(R.id.ForSync);
		mTab2 = (LinearLayout) findViewById(R.id.Synced);
		t1 = (TextView) findViewById(R.id.ForSyncfile);
		t2 = (TextView) findViewById(R.id.SyncedFile);
		upload = (Button) findViewById(R.id.Syncbutton);
		syncfile = new ArrayList<File>();
		if (findViewById(R.id.fragment_place) != null) {
			if (savedInstanceState != null) {
				selected_tab = savedInstanceState.getInt("selected", 1);
				return;
			}

			mFragment_File_For_sync = new Fragmentsync();
			mFragment_Synced_File = new Fragment_Synced_File();

			my_fragment_manager = getSupportFragmentManager();
			fragmentTransaction = my_fragment_manager.beginTransaction();
			fragmentTransaction.replace(R.id.fragment_place,
					mFragment_Synced_File);
			fragmentTransaction.addToBackStack(null);
			fragmentTransaction.commit();
			mTab1.setBackgroundColor(Color.TRANSPARENT);
			t1.setTextColor(getResources().getColor(R.color.line_color));
			mTab2.setBackgroundResource(R.drawable.shadow);
			t2.setTextColor(getResources().getColor(R.color.white));
		}
		mBack.setOnClickListener(this);
		mTab1.setOnClickListener(this);
		mTab2.setOnClickListener(this);
		upload.setOnClickListener(this);
		my_fragment_manager
				.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {

					@Override
					public void onBackStackChanged() {
						// TODO Auto-generated method stub
						Log.e("", "back");

						Call_back_button();
					}
				});

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.backLinear1:
			Log.e("Click", "Click");
			finish();
			break;

		case R.id.ForSync:
			selected_tab = 1;
			fragmentTransaction = my_fragment_manager.beginTransaction();
			fragmentTransaction.replace(R.id.fragment_place,
					mFragment_File_For_sync);
			fragmentTransaction.addToBackStack(null);
			mTab1.setBackgroundResource(R.drawable.shadow);
			t1.setTextColor(getResources().getColor(R.color.white));
			mTab2.setBackgroundColor(Color.TRANSPARENT);
			t2.setTextColor(getResources().getColor(R.color.line_color));
			fragmentTransaction.commit();
			break;
		case R.id.Synced:
			selected_tab = 2;
			fragmentTransaction = my_fragment_manager.beginTransaction();
			fragmentTransaction.replace(R.id.fragment_place,
					mFragment_Synced_File);
			fragmentTransaction.addToBackStack(null);
			mTab1.setBackgroundColor(Color.TRANSPARENT);
			t1.setTextColor(getResources().getColor(R.color.line_color));
			mTab2.setBackgroundResource(R.drawable.shadow);
			t2.setTextColor(getResources().getColor(R.color.white));
			fragmentTransaction.commit();
			break;
		case R.id.Syncbutton:
			try {
				syncfile = mFragment_File_For_sync.getModifyList();
				Log.e("syncfile", "" + syncfile);

				if (syncfile.size() != 0) {

					File[] Files = new File[syncfile.size()];
					syncfile.toArray(Files);
					Log.e("Files", "" + Files);
					UploadPicture upload = new UploadPicture(mContext, mApi,
							PHOTO_DIR, Files);
					Log.e("upload", "" + upload);
					upload.execute();

				} else {
					Toast.makeText(getApplicationContext(),
							"Please Select at Least One Item",
							Toast.LENGTH_SHORT).show();
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;
		}
	}

	@Override
	public void onBackPressed() {
		my_fragment_manager.popBackStack();
		if (mFragment_Synced_File.isVisible()) {
			finish();

		}
	}

	private AndroidAuthSession buildSession() {
		AppKeyPair appKeyPair = new AppKeyPair(APP_KEY, APP_SECRET);

		AndroidAuthSession session = new AndroidAuthSession(appKeyPair);
		loadAuth(session);
		return session;
	}

	private void loadAuth(AndroidAuthSession session) {
		SharedPreferences prefs = getSharedPreferences(ACCOUNT_PREFS_NAME, 0);
		String key = prefs.getString(ACCESS_KEY_NAME, null);
		String secret = prefs.getString(ACCESS_SECRET_NAME, null);
		if (key == null || secret == null || key.length() == 0
				|| secret.length() == 0)
			return;

		if (key.equals("oauth2:")) {
			// If the key is set to "oauth2:", then we can assume the token is
			// for OAuth 2.
			session.setOAuth2AccessToken(secret);
		} else {
			// Still support using old OAuth 1 tokens.
			session.setAccessTokenPair(new AccessTokenPair(key, secret));
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		outState.putInt("selected", selected_tab);

	}

	public void Call_back_button() {

		if (mFragment_File_For_sync.isVisible()) {
			mTab1.setBackgroundResource(R.drawable.shadow);
			t1.setTextColor(getResources().getColor(R.color.white));
			mTab2.setBackgroundColor(Color.TRANSPARENT);
			t2.setTextColor(getResources().getColor(R.color.line_color));
			selected_tab = 1;

		} else if (mFragment_Synced_File.isVisible()) {
			mTab1.setBackgroundColor(Color.TRANSPARENT);
			t1.setTextColor(getResources().getColor(R.color.line_color));
			mTab2.setBackgroundResource(R.drawable.shadow);
			t2.setTextColor(getResources().getColor(R.color.white));
			selected_tab = 2;

		}

	}

	public class UploadPicture extends AsyncTask<Void, Long, Boolean> {

		private DropboxAPI<?> mApi;
		private String mPath;

		private UploadRequest mRequest;
		private Context mContext;
		private ProgressDialog mDialog;
		File file;
		private String mErrorMsg;
		int indBytes;
		// new class variables:
		private int mFilesUploaded;
		private File[] mFilesToUpload;
		private int mCurrentFileIndex;

		public UploadPicture(Context context, DropboxAPI<?> api,
				String dropboxPath, File[] filesToUpload) {
			// We set the context this way so we don't accidentally leak
			// activities
			mContext = context;
			mApi = api;
			mPath = dropboxPath;
			// set number of files uploaded to zero.
			mCurrentFileIndex = 0;
			mFilesUploaded = 0;
			mFilesToUpload = filesToUpload;
			mDialog = new ProgressDialog(mContext);
			mDialog.setMax(100);
			Log.e("Uploading file " + mFilesUploaded++, "/"
					+ mFilesToUpload.length);
			mDialog.setMessage("Uploading file  " + mFilesToUpload.length);
			mDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			mDialog.setProgress(0);
			mDialog.setCanceledOnTouchOutside(false);
			// mDialog.setButton("Cancel", new OnClickListener() {
			// public void onClick(DialogInterface dialog, int which) {
			// cancel(true);
			// }
			// });
			mDialog.show();
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			try {
				for (int i = 0; i < mFilesToUpload.length; i++) {
					file = mFilesToUpload[i];
					Log.e("file", "" + file);
					int bytes = (int) mFilesToUpload[i].length();
					indBytes = bytes;
					// By creating a request, we get a handle to the putFile
					// operation,
					// so we can cancel it later if we want to
					FileInputStream fis = new FileInputStream(file);
					String path = mPath + file.getName();
					mRequest = mApi.putFileOverwriteRequest(path, fis,
							file.length(), new ProgressListener() {
								@Override
								public long progressInterval() {
									// Update the progress bar every half-second
									// or
									// so
									return 500;
								}

								@Override
								public void onProgress(long bytes, long total) {
									if (isCancelled()) {
										// This will cancel the putFile
										// operation
										mRequest.abort();
									} else {
										publishProgress(bytes);
									}
								}
							});

					mRequest.upload();

					if (!isCancelled()) {
						mFilesUploaded++;
					} else {
						return false;
					}
				}
				return true;
			} catch (DropboxUnlinkedException e) {
				// This session wasn't authenticated properly or user unlinked
				mErrorMsg = "This app wasn't authenticated properly.";
			} catch (DropboxFileSizeException e) {
				// File size too big to upload via the API
				mErrorMsg = "This file is too big to upload";
			} catch (DropboxPartialFileException e) {
				// We canceled the operation
				mErrorMsg = "Upload canceled";
			} catch (DropboxServerException e) {
				// Server-side exception. These are examples of what could
				// happen,
				// but we don't do anything special with them here.
				if (e.error == DropboxServerException._401_UNAUTHORIZED) {
					// Unauthorized, so we should unlink them. You may want to
					// automatically log the user out in this case.
				} else if (e.error == DropboxServerException._403_FORBIDDEN) {
					// Not allowed to access this
				} else if (e.error == DropboxServerException._404_NOT_FOUND) {
					// path not found (or if it was the thumbnail, can't be
					// thumbnailed)
				} else if (e.error == DropboxServerException._507_INSUFFICIENT_STORAGE) {
					// user is over quota
				} else {
					// Something else
				}
				// This gets the Dropbox error, translated into the user's
				// language
				mErrorMsg = e.body.userError;
				if (mErrorMsg == null) {
					mErrorMsg = e.body.error;
				}
			} catch (DropboxIOException e) {
				// Happens all the time, probably want to retry automatically.
				mErrorMsg = "Network error.  Try again.";
			} catch (DropboxParseException e) {
				// Probably due to Dropbox server restarting, should retry
				mErrorMsg = "Dropbox error.  Try again.";
			} catch (DropboxException e) {
				// Unknown error
				mErrorMsg = "Unknown error.  Try again.";
			} catch (FileNotFoundException e) {
			}
			return false;
		}

		@Override
		protected void onProgressUpdate(Long... progress) {

			// mDialog.setMessage("Uploading file " + (mCurrentFileIndex+1) +
			// " / " + mFilesToUpload.length);

			int percent = (int) (100.0 * (double) progress[0] / indBytes + 0.5);

			Log.e("onProgressUpdate", "" + file.getName());
			mDialog.setMessage("Uploading file :" + file.getName());
			Log.e("pro", percent + "    " + progress[0] + "/" + indBytes);
			mDialog.setProgress(percent);
		}

		@Override
		protected void onPostExecute(Boolean result) {
			mDialog.dismiss();

			if (result) {
				showToast("File successfully uploaded");
				selected_tab = 2;
				fragmentTransaction = my_fragment_manager.beginTransaction();
				fragmentTransaction.replace(R.id.fragment_place,
						mFragment_Synced_File);
				fragmentTransaction.addToBackStack(null);
				mTab1.setBackgroundColor(Color.TRANSPARENT);
				t1.setTextColor(getResources().getColor(R.color.line_color));
				mTab2.setBackgroundResource(R.drawable.shadow);
				t2.setTextColor(getResources().getColor(R.color.white));
				fragmentTransaction.commit();
			} else {
				showToast(mErrorMsg);
			}

		}

		private void showToast(String msg) {
			Toast error = Toast.makeText(mContext, msg, Toast.LENGTH_LONG);
			error.show();
		}
	}

}
