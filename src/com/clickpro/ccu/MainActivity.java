package com.clickpro.ccu;

import net.tsz.afinal.FinalBitmap;

import com.clickpro.client.GetImageThread;
import com.clickpro.client.HttpUtil;
import com.clickpro.client.ParseHtml;
import com.clickpro.util.CommonUtil;
import com.clickpro.util.Global;
import com.clickpro.view.CustomDialog;
import com.clickpro.view.MySurfaceView;
import com.clickpro.view.MySurfaceView.ChangeSizeListener;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;

/**
 * �׽���
 * 
 * @description
 * @version 1.0
 * @update 2013-10-10 ����7:49:21
 */
public class MainActivity extends Activity implements OnClickListener,
		ChangeSizeListener {
	private Button setting, scanPic, play;
	private Context mContext;
	private Button videoCamera, refresh;
	private TextView resulation;
	private ProgressBar pb;
	private ImageView redPoint;
	private ImageView preview;
	private LinearLayout photographLinear, lianpai, cameraLinear;
	/** �ײ��м䰴ť״̬ 0:���� 1:���� 2:��ʼ¼�� 3:¼��ֹͣ **/
	private int status = 2;
	/** ��ʾ��ǰʱ�� **/
	private TextView currentTime;
	private PopupWindow popwindow;
	private View popView;
	private RelativeLayout parent;
	private CustomDialog dialog;
	private Chronometer chronometer;
	private ImageView sx;
	/** ����Ƿ���˸ **/
	private boolean isVisible = false;
	private MySurfaceView mSurfaceView;
	/** ��Ļ�� **/
	private int width;
	private ScreenStatus screenStatus;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		screenStatus = new ScreenStatus();
		IntentFilter filter = new IntentFilter();
		filter.addAction(Intent.ACTION_SCREEN_ON);
		filter.addAction(Intent.ACTION_SCREEN_OFF);
		filter.addAction(Intent.ACTION_USER_PRESENT);
		registerReceiver(screenStatus, filter);
		mContext = this;
		width = getWindowManager().getDefaultDisplay().getWidth();
		initView();
		new ConnectionAsy(1).execute();
		new ControlImage().start();
		// DownloadManager.Query baseQuery = new DownloadManager.Query()
		// .setOnlyIncludeVisibleInDownloadsUi(true);
		// baseQuery.setFilterByStatus(Downloads.STATUS_SUCCESS);
		// DownloadManager mDownloadManager = new
		// DownloadManager(getContentResolver(),
		// getPackageName());
		// Cursor cursor=mDownloadManager.query(baseQuery);
		// cursor.moveToFirst();
		// while(!cursor.isAfterLast()){
		// String
		// url=cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_URI));
		// cursor.moveToNext();
		// Log.e("990", "url==="+url);
		// }

	}

	private void initView() {
		FinalBitmap.create(mContext).configLoadingImage(
				R.drawable.default_photo);
		FinalBitmap.create(mContext).configRecycleImmediately(false);
		setting = (Button) findViewById(R.id.setting);
		setting.setOnClickListener(this);
		videoCamera = (Button) findViewById(R.id.videoCamera);
		videoCamera.setOnClickListener(this);
		scanPic = (Button) findViewById(R.id.scanPic);
		scanPic.setOnClickListener(this);
		resulation = (TextView) findViewById(R.id.videoResulation);
		pb = (ProgressBar) findViewById(R.id.pb);
		play = (Button) findViewById(R.id.play);
		play.setOnClickListener(this);
		preview = (ImageView) findViewById(R.id.preview);
		popView = LayoutInflater.from(mContext)
				.inflate(R.layout.pop_view, null);
		popView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				closePopupWindow();
				return false;
			}
		});
		photographLinear = (LinearLayout) popView
				.findViewById(R.id.photographLinear);
		lianpai = (LinearLayout) popView.findViewById(R.id.conLinear);
		cameraLinear = (LinearLayout) popView.findViewById(R.id.cameraLinear);

		photographLinear.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				chronometer.setVisibility(View.INVISIBLE);
				closePopupWindow();
				status = 0;
				if (Global.burstMode != null && !Global.burstMode.equals("off")) {
					resulation.setText(Global.burstMode + " photos");
					sx.setBackgroundResource(R.drawable.lianpai);
					play.setBackgroundResource(R.drawable.lianpai_doing);
				} else {
					if (Global.photoResulation != null) {
						resulation.setText(Global.photoResulation);
					}
					sx.setBackgroundResource(R.drawable.zxj);
					play.setBackgroundResource(R.drawable.take_phone);
				}

			}
		});
		lianpai.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				chronometer.setVisibility(View.INVISIBLE);
				closePopupWindow();
				status = 1;
				sx.setBackgroundResource(R.drawable.lianpai);
				play.setBackgroundResource(R.drawable.lianpai_doing);
				if (Global.photoResulation != null) {
					resulation.setText(Global.photoResulation);
				}
			}
		});
		cameraLinear.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				chronometer.setVisibility(View.VISIBLE);
				closePopupWindow();
				status = 2;
				sx.setBackgroundResource(R.drawable.shexiang);
				play.setBackgroundResource(R.drawable.play_btn);
				if (Global.vResulation != null) {
					resulation.setText(Global.vResulation);
				}
			}
		});
		redPoint = (ImageView) findViewById(R.id.redPoint);
		currentTime = (TextView) findViewById(R.id.currentTime);
		parent = (RelativeLayout) findViewById(R.id.parent);
		chronometer = (Chronometer) findViewById(R.id.chronometer);

		chronometer.setFormat("%s");
		refresh = (Button) findViewById(R.id.refresh);
		refresh.setOnClickListener(this);
		sx = (ImageView) findViewById(R.id.sx);

		mSurfaceView = (MySurfaceView) findViewById(R.id.mSurfaceView);
		mSurfaceView.setZOrderOnTop(true);
		mSurfaceView.getHolder().setFormat(PixelFormat.TRANSLUCENT);
		mSurfaceView.setChangeSizeListener(this);
		mSurfaceView.setOnClickListener(this);

	}

	private void setSurfaceViewSize(float scale, int bmpHeight) {
		LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) mSurfaceView
				.getLayoutParams();
		lp.width = width;
		lp.height = (int) (bmpHeight * scale);
		mSurfaceView.setLayoutParams(lp);
		mSurfaceView.invalidate();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent intent;
		switch (v.getId()) {
		case R.id.setting:
			if (status == 3) {
				showAlertDialog();
			} else {
				intent = new Intent(mContext, SettingActivity.class);
				startActivity(intent);
				overridePendingTransition(0, 0);
			}

			break;
		case R.id.videoCamera:
			if (status == 3) {
				showAlertDialog();
			} else {
				if (popwindow != null && popwindow.isShowing()) {
					closePopupWindow();
				} else {
					initPopupWindow(popView);
				}
			}

			break;
		case R.id.scanPic:
			if (status == 3) {
				showAlertDialog();
			} else {
				intent = new Intent(mContext, PlayBackActivity.class);
				startActivity(intent);
				overridePendingTransition(0, 0);
			}
			break;
		case R.id.play:
			if (status == 0) {
				// ����
				new Request(Global.takePhoto).execute();
			} else if (status == 2) {
				// ��ʼ¼��
				new Request(Global.startRecord).execute();
				play.setBackgroundResource(R.drawable.start_video);
				status = 3;
			} else if (status == 3) {
				// ֹͣ¼��
				new Request(Global.stopRecord).execute();
				play.setBackgroundResource(R.drawable.play_btn);
				status = 2;
				isVisible = false;
				chronometer.stop();
				chronometer.setBase(SystemClock.elapsedRealtime());
			}
			break;
		case R.id.mSurfaceView:
			if (Global.isConn) {
				// ���������¼���״̬���Ͳ���ˢ��Ԥ������
				if (status != 3) {
					new Request(Global.resetvf).execute();
				}
			} else {
				new ConnectionAsy(0).execute();
			}

			break;
		case R.id.refresh:
			new ConnectionAsy(0).execute();
			break;
		default:
			break;
		}
	}

	Handler mHandler = new Handler() {

		@SuppressLint("NewApi")
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case 0:
				// Ԥ������ ͼƬ���л�
				preview.setImageBitmap((Bitmap) msg.obj);
				// myphoto.setBitmap(bitmap);
				// myphoto.postInvalidate();
				break;
			case 1:
				// ��ʾ��ǰʱ��
				String date = (String) msg.obj;
				if (date != null) {
					currentTime.setText(date);
				}
				break;
			case 2:
				resulation.setVisibility(View.GONE);
				pb.setVisibility(View.VISIBLE);
				break;
			case 3:
				resulation.setVisibility(View.VISIBLE);
				pb.setVisibility(View.GONE);

				if (status == 0) {
					if (Global.burstMode != null
							&& !Global.burstMode.equals("0")) {
						chronometer.setVisibility(View.INVISIBLE);
						closePopupWindow();
						sx.setBackgroundResource(R.drawable.lianpai);
						resulation.setText(Global.burstMode + " photos");
					} else {
						if (Global.photoResulation != null) {
							resulation.setText(Global.photoResulation);
						} else {
							resulation.setText("");
						}
					}

				} else {
					if (Global.vResulation != null) {
						resulation.setText(Global.vResulation);
					} else {
						resulation.setText("");
					}
				}
				break;
			case 4:
				redPoint.setVisibility(View.VISIBLE);
				break;
			case 5:
				redPoint.setVisibility(View.INVISIBLE);
				break;
			case 6:
				float scale = (Float) msg.obj;
				int bmpHeight = msg.arg1;
				setSurfaceViewSize(scale, bmpHeight);
				break;
			default:
				break;
			}

		};
	};

	/***
	 * ������
	 * 
	 * @version 1.0
	 * @author ������
	 * @update 2013-11-22 ����11:33:22
	 */
	private void showAlertDialog() {
		AlertDialog.Builder buidler = new AlertDialog.Builder(mContext);
		buidler.setMessage(getString(R.string.stop_record));

		buidler.setNegativeButton(getString(R.string.sure),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				});
		buidler.show();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MySurfaceView.isRun = false;
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		MySurfaceView.isRun = false;
		unregisterReceiver(screenStatus);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (status == 0) {
			if (Global.burstMode != null && !Global.burstMode.equals("off")) {
				resulation.setText(Global.burstMode + " photos");
				sx.setBackgroundResource(R.drawable.lianpai);
				play.setBackgroundResource(R.drawable.lianpai_doing);
			} else {
				if (Global.photoResulation != null) {
					resulation.setText(Global.photoResulation);
				}
				sx.setBackgroundResource(R.drawable.zxj);
				play.setBackgroundResource(R.drawable.take_phone);
			}
		} else {
			resulation.setText(Global.vResulation);
		}
	};

	class ControlImage extends Thread {
		@Override
		public void run() {
			// TODO Auto-generated method stub
			super.run();
			while (true) {
				if (isVisible) {
					mHandler.obtainMessage(4).sendToTarget();
					try {
						Thread.sleep(500);
						mHandler.obtainMessage(5).sendToTarget();
						Thread.sleep(500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}
		}
	}

	/**
	 * ����
	 * 
	 * @description
	 * @version 1.0
	 * @author ������
	 * @update 2013-11-8 ����1:39:41
	 */
	class ConnectionAsy extends AsyncTask<String, String, String> {
		int resultCode;
		int first = 0;

		public ConnectionAsy(int first) {
			// TODO Auto-generated constructor stub
			this.first = 0;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = new CustomDialog(mContext);
			dialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			String startUrl = "http://192.168.42.1/cgi-bin/cgi?CMD=SESSION_START";
			resultCode = HttpUtil.getResponse(startUrl);
			if (resultCode == 200) {
				new GetImageThread(mHandler).start();
				new GetResulation().start();
			}
			return null;

		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (dialog != null && dialog.isShowing()) {
				dialog.dismiss();
			}
			refresh.clearAnimation();
			if (resultCode == 200) {
				CommonUtil.showToast(mContext,
						getString(R.string.connection_successful));
			} else {
				CommonUtil.showToast(mContext,
						getString(R.string.connection_failed));
			}

		}

	}

	/**
	 * ��ȡ��Ƶ�ֱ��ʺ�ͼƬ�ֱ���
	 * 
	 * @description
	 * @version 1.0
	 * @author ������
	 * @update 2013-11-29 ����12:06:44
	 */
	class GetResulation extends Thread {
		@Override
		public void run() {
			// TODO Auto-generated method stub
			super.run();
			mHandler.obtainMessage(2).sendToTarget();
			Global.hashmap = ParseHtml.parseSet();
			if (Global.hashmap != null && Global.hashmap.size() > 0) {
				Global.vResulation = Global.hashmap.get(0).get(0);
				Global.photoResulation = Global.hashmap.get(1).get(0);
				Global.dualStreams = Global.hashmap.get(2).get(0);
				Global.videoStamp = Global.hashmap.get(3).get(0);
				Global.photoStampValue = Global.hashmap.get(4).get(0);
				Global.burstMode = Global.hashmap.get(5).get(0);
			}
			Intent intent = new Intent("data");
			sendBroadcast(intent);
			mHandler.obtainMessage(3).sendToTarget();

		}
	}

	/**
	 * ����
	 * 
	 * @description
	 * @version 1.0
	 * @author ������
	 * @update 2013-11-8 ����1:39:41
	 */
	class Request extends AsyncTask<String, String, String> {
		String url;
		int resultCode;

		public Request(String url) {
			// TODO Auto-generated constructor stub
			this.url = url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = new CustomDialog(mContext);
			dialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			resultCode = HttpUtil.getResponse(url);
			return null;

		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (dialog != null && dialog.isShowing()) {
				dialog.dismiss();
			}
			if (resultCode == 200) {
				if (status == 3) {
					isVisible = true;
					chronometer.setBase(SystemClock.elapsedRealtime());
					chronometer.start();
				}
				CommonUtil.showCustomToast(mContext);
			} else {
				CommonUtil
						.showToast(mContext, getString(R.string.request_fail));
			}

		}

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		closePopupWindow();
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * @description ����popupwindow
	 * @version 1.0
	 * @author ������
	 * @update 2013-10-17 ����2:52:16
	 */
	private void initPopupWindow(View resId) {
		if (popwindow == null) {
			popwindow = new PopupWindow(resId,
					LinearLayout.LayoutParams.WRAP_CONTENT,
					LinearLayout.LayoutParams.WRAP_CONTENT);
		}
		popwindow.showAtLocation(parent, Gravity.BOTTOM
				| Gravity.CENTER_HORIZONTAL, 0, 80);
		popwindow.update();
	}

	private void closePopupWindow() {
		if (popwindow != null && popwindow.isShowing()) {
			popwindow.dismiss();
		}
	}

	/***
	 * ������Ļ��״̬
	 * 
	 * @description
	 * @version 1.0
	 * @author weixi liang
	 * 
	 */
	class ScreenStatus extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			if (intent.getAction().equalsIgnoreCase(Intent.ACTION_SCREEN_ON)) {
				System.out.println("����");
				MySurfaceView.isRun = true;
			} else if (intent.getAction().equalsIgnoreCase(
					Intent.ACTION_SCREEN_OFF)) {
				System.out.println("����");
				MySurfaceView.isRun = false;
			} else if (intent.getAction().equalsIgnoreCase(
					Intent.ACTION_USER_PRESENT)) {
				System.out.println("����");
				new ConnectionAsy(0).execute();
			}
		}

	}

	@Override
	public void changeSize(float scale, int bmpHeight) {
		// TODO Auto-generated method stub
		Message msg = mHandler.obtainMessage();
		msg.what = 6;
		msg.arg1 = bmpHeight;
		msg.obj = scale;
		mHandler.sendMessage(msg);
	}

}
