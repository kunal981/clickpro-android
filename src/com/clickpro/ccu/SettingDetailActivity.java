package com.clickpro.ccu;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import com.clickpro.adapter.SettingDetailAdapter;
import com.clickpro.client.HttpUtil;
import com.clickpro.util.CommonUtil;
import com.clickpro.util.Global;
import com.clickpro.view.CustomDialog;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;

/**
 * �����������
 * 
 * @description
 * @version 1.0
 * @update 2013-10-10 ����7:49:08
 */
public class SettingDetailActivity extends BaseActivity {
	private ListView listview;
	private SettingDetailAdapter adapter;
	private ArrayList<String> lists;
	private Context mContext;
	private LinearLayout back;
	private int position;
	/** ���õ�ֵ **/
	private String param;
	private String titleText;
	private TextView title;
	private String value;
	/** ��ǰ�ڼ��ѡ�� **/
	private int index;
	/** �ֶ�ѡ��ڼ��� **/
	private int currentIndex;
	private Button refresh;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting);
		mContext = this;

		lists = getIntent().getStringArrayListExtra("list");
		titleText = getIntent().getStringExtra("title");
		value = getIntent().getStringExtra("value");
		position = getIntent().getIntExtra("position", 0);
		if (lists != null) {
			for (int i = 0; i < lists.size(); i++) {
				if (value.trim().equalsIgnoreCase(lists.get(i))) {
					index = i;
				}
			}
		}
		currentIndex = index;
		initView();
	}

	private void initView() {
		// TODO Auto-generated method stub
		refresh = (Button) findViewById(R.id.refresh);
		refresh.setVisibility(View.INVISIBLE);
		title = (TextView) findViewById(R.id.title);
		title.setText(titleText);
		back = (LinearLayout) findViewById(R.id.back);
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (currentIndex != index) {
					saveSetting(param);
				} else {
					finish();
				}

			}
		});
		listview = (ListView) findViewById(R.id.listview);
		adapter = new SettingDetailAdapter(mContext, lists);
		adapter.setSelectedPisition(index);
		listview.setAdapter(adapter);
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				adapter.setSelectedPisition(arg2);
				adapter.notifyDataSetChanged();
				currentIndex = arg2;
				param = (String) lists.get(arg2);
			}
		});
	}

	/***
	 * ��ʾ�Ƿ񱣴�����
	 * 
	 * @version 1.0
	 * @author ������
	 * @update 2013-11-29 ����9:41:30
	 */
	private void saveSetting(final String param) {
		AlertDialog.Builder builder = new Builder(mContext);
		builder.setMessage(getString(R.string.save_setting));
		builder.setPositiveButton(getString(R.string.no),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						finish();
					}
				});
		builder.setNegativeButton(getString(R.string.yes),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						if (param != null) {
							doExecute(param);
						} else {
							finish();
						}

					}
				});
		builder.show();
	}

	/**
	 * ִ������
	 * 
	 * @param params
	 * @version 1.0
	 * @author ������
	 * @update 2013-11-29 ����9:53:59
	 */
	private void doExecute(String params) {
		try {
			params = URLEncoder.encode(params, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		switch (position) {
		case 0:
			params = Global.setVideoUrl + params;
			request(params);
			break;
		case 1:
			params = Global.setPhotoUrl + params;
			request(params);
			break;
		case 2:
			params = Global.setDualStream + params;
			request(params);
			break;
		case 3:
			params = Global.setVideoStamp + params;
			request(params);
			break;
		case 4:
			params = Global.setPhotoStamp + params;
			request(params);
			break;
		case 5:
			params = Global.setBurstMode + params;
			request(params);
			break;
		default:
			break;
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (currentIndex != index) {
				saveSetting(param);
			} else {
				finish();
			}

		}
		return false;
	}

	private void request(String param) {
		new Request(param).execute();
	}

	/**
	 * @description
	 * @version 1.0
	 * @author ������
	 * @update 2013-11-8 ����1:39:41
	 */
	class Request extends AsyncTask<String, String, String> {
		String path;
		int resultCode;
		CustomDialog dialog;

		public Request(String path) {
			// TODO Auto-generated constructor stub
			this.path = path;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = new CustomDialog(mContext);
			dialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			HttpUtil.getResponse(Global.setting);
			resultCode = HttpUtil.getResponse(path);
			if (resultCode == 200) {
				if (position == 0) {
					Global.vResulation = param;
				} else if (position == 1) {
					Global.photoResulation = param;
				} else if (position == 2) {
					Global.dualStreams = param;
				} else if (position == 3) {
					Global.videoStamp = param;
				} else if (position == 4) {
					Global.photoStampValue = param;
				} else if (position == 5) {
					Global.burstMode = param;
				}
			}

			return null;

		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (dialog != null && dialog.isShowing()) {
				dialog.dismiss();
			}
			new BackCmd().start();
			if (resultCode == 200) {
				CommonUtil.showCustomToast(mContext);
				finish();
			} else {
				CommonUtil
						.showToast(mContext, getString(R.string.request_fail));
			}

		}

	}

	class BackCmd extends Thread {
		@Override
		public void run() {
			// TODO Auto-generated method stub
			super.run();
			HttpUtil.getResponse(Global.backcmd);
		}
	}
}
