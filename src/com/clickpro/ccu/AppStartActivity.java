package com.clickpro.ccu;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import com.clickpro.util.MySharedPreferences;

public class AppStartActivity extends Activity {
	private Handler mHandler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		View view = View.inflate(this, R.layout.start, null);

		try {
			PackageInfo info = getPackageManager().getPackageInfo(
					"com.clickpro.ccu", PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Log.e("KeyHash:",
						Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (NameNotFoundException e) {

		} catch (NoSuchAlgorithmException e) {

		}
		Log.e("test:", "te4wst");
		setContentView(view);
		mHandler = new Handler();
		mHandler.postDelayed(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				String isFirst = MySharedPreferences.getStringValue(
						AppStartActivity.this, MySharedPreferences.isFirst);
				if (isFirst != null && !isFirst.equals("")) {
					// ����������
					Intent intent = new Intent(AppStartActivity.this,
							ChoiceActivity.class);
					startActivity(intent);
					finish();
					overridePendingTransition(0, 0);
				} else {
					Intent intent = new Intent(AppStartActivity.this,
							SplashActivity.class);
					startActivity(intent);
					finish();
					overridePendingTransition(0, 0);
				}

			}
		}, 3000);
	}

}
