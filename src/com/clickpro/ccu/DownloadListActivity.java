package com.clickpro.ccu;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.clickpro.adapter.ViewPagerAdapter;
import com.clickpro.ccu.DownloadListActivity.DownloadListAdapter.DownloadViewHoldler;
import com.clickpro.entity.DownloadInfo;
import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.android.AuthActivity;
import com.dropbox.client2.session.AccessTokenPair;
import com.dropbox.client2.session.AppKeyPair;
import com.facebook.Session;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.DbException;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.HttpHandler;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.pinterest.pinit.PinItButton;
import com.pinterest.pinit.PinItListener;

public class DownloadListActivity extends FragmentActivity implements
		OnClickListener {
	/*
	 * Twitter
	 */

	/* Shared preference keys */
	private static final String PREF_NAME = "sample_twitter_pref";
	private static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
	private static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
	private static final String PREF_KEY_TWITTER_LOGIN = "is_twitter_loggedin";
	private static final String PREF_USER_NAME = "twitter_user_name";

	// for Drop Box Authentication
	private static final String APP_KEY = "vms1kkpy97ducky";
	private static final String APP_SECRET = "878suyz710yqxtn";

	private static final String ACCOUNT_PREFS_NAME = "prefs";
	private static final String ACCESS_KEY_NAME = "ACCESS_KEY";
	private static final String ACCESS_SECRET_NAME = "ACCESS_SECRET";
	private boolean mLoggedIn;
	private static final boolean USE_OAUTH1 = false;
	/* Any number for uniquely distinguish your request */
	public static final int WEBVIEW_REQUEST_CODE = 100;

	private ProgressDialog mDialogTwitter;

	private static Twitter twitter;
	private static RequestToken requestToken;

	private static SharedPreferences mSharedPreferences;

	private TextView userName;
	private View loginLayout;
	private View shareLayout;

	private String consumerKey = null;
	private String consumerSecret = null;
	private String callbackUrl = null;
	private String oAuthVerifier = null;
	DownloadInfo downloadInfo;
	boolean isLoggedIn;

	/*
	 * Pinterest ID
	 */
	PinItButton pinIt;
	public static final String CLIENT_ID = "1445170";

	private DownloadListAdapter downloadingAdapter;
	private DownloadManager downloadManager;
	private RelativeLayout backLinear;
	private LinearLayout photographLinear, videoLinear;
	private ListView downloading, downloaded;
	private TextView photographText, videoText;
	ImageView Foruploading;
	private Context mContext;
	private List<View> views = new ArrayList<View>();
	private ViewPager viewPager;
	private List<DownloadInfo> downloadedInfoList = new ArrayList<DownloadInfo>();
	DownloadedAdapter downloadedAdapter;
	DropboxAPI<AndroidAuthSession> mApi;
	private UiLifecycleHelper uiHelper;
	private static final List<String> PERMISSIONS = Arrays
			.asList("publish_actions");
	private static final String PENDING_PUBLISH_KEY = "pendingPublishReauthorization";
	private boolean pendingPublishReauthorization = false;
	private final String PHOTO_DIR = "/TEST/";
	ArrayList<String> Sync = new ArrayList<String>();

	File file1;

	// private Session.StatusCallback callback = new Session.StatusCallback() {
	// @Override
	// public void call(Session session, SessionState state,
	// Exception exception) {
	// if (state.isOpened()) {
	// Log.d("FacebookActivity", "Facebook session opened");
	// } else if (state.isClosed()) {
	// Log.d("FacebookActivity", "Facebook session closed");
	// }
	// onSessionStateChange(session, state, exception);
	// // onSessionStateChange(session, state, exception);
	// }
	// };
	//
	// private FacebookDialog.Callback dialogCallback = new
	// FacebookDialog.Callback() {
	// @Override
	// public void onError(FacebookDialog.PendingCall pendingCall,
	// Exception error, Bundle data) {
	// Log.d("", String.format("Error: %s", error.toString()));
	// }
	//
	// @Override
	// public void onComplete(FacebookDialog.PendingCall pendingCall,
	// Bundle data) {
	// Log.d("", "Success!");
	// Toast.makeText(DownloadListActivity.this,
	// "Successfully Posted", Toast.LENGTH_SHORT).show();
	// // final String postId = data
	// // .getString("com.facebook.platform.extra.COMPLETION_GESTURE");
	// // if (postId.equalsIgnoreCase("post")) {
	// // // Toast.makeText(getActivity(),
	// // // "Restaurant successfully shared",
	// // // Toast.LENGTH_SHORT).show();
	// // Toast.makeText(DownloadListActivity.this,
	// // "Successfully Posted", Toast.LENGTH_SHORT).show();
	// // Log.d("", "Restaurant successfully shared");
	// // } else if (postId.equalsIgnoreCase("cancel")) {
	// // Toast.makeText(DownloadListActivity.this, "Cancelled",
	// // Toast.LENGTH_SHORT).show();
	// //
	// // }
	// }
	// };

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		/* initializing twitter parameters from string.xml */
		initTwitterConfigs();

		/* Enabling strict mode */
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);
		setContentView(R.layout.download_list);
		AndroidAuthSession session = buildSession();
		Log.e("session", "" + session.toString());
		mApi = new DropboxAPI<AndroidAuthSession>(session);

		Foruploading = (ImageView) findViewById(R.id.Upload);
		Foruploading.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				isNetworkAvailable(h, 2000);

			}

		});

		mContext = this;
		super.onCreate(savedInstanceState);
		/**
		 * For Facebook integration
		 */
		uiHelper = new UiLifecycleHelper(this, null);
		uiHelper.onCreate(savedInstanceState);

		/**
		 * For Pinterest integration
		 */

		PinItButton.setPartnerId(CLIENT_ID);
		PinItButton.setDebugMode(true);

		/**
		 * For twitter Check if required twitter keys are set
		 */

		mDialogTwitter = new ProgressDialog(this);
		if (TextUtils.isEmpty(consumerKey) || TextUtils.isEmpty(consumerSecret)) {
			Toast.makeText(this, "Twitter key and secret not configured",
					Toast.LENGTH_SHORT).show();
			return;
		}

		/* *
		 * For twitter Initialize application preferences
		 */
		mSharedPreferences = getSharedPreferences(PREF_NAME, 0);

		isLoggedIn = mSharedPreferences.getBoolean(PREF_KEY_TWITTER_LOGIN,
				false);

		downloadManager = DownloadService.getDownloadManager(mContext);
		initView();
	}

	Handler h = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			if (msg.what != 1) { // code if not connected

				Log.e("Internet", "Noaccessibility");
				AlertDialog.Builder builder = new AlertDialog.Builder(
						DownloadListActivity.this);
				builder = new AlertDialog.Builder(DownloadListActivity.this);
				builder.setCancelable(false)
						.setMessage("Please Connect with Other Network")
						.setNegativeButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								});

				AlertDialog alertDialog = builder.create();
				alertDialog.show();

			} else { // code if connected
				Log.e("Internet", "Accessibility");
				if (mLoggedIn) {

					Intent intent = new Intent(getApplicationContext(),
							DropBox_File.class);
					startActivity(intent);

					// logOut();
				} else {

					Intent intent = new Intent(getApplicationContext(),
							SettingActivity.class);
					startActivity(intent);

				}
			}
		}
	};

	// For Check Internet Access
	public static void isNetworkAvailable(final Handler handler,
			final int timeout) {
		// ask fo message '0' (not connected) or '1' (connected) on 'handler'
		// the answer must be send before before within the 'timeout' (in
		// milliseconds)

		new Thread() {
			private boolean responded = false;

			@Override
			public void run() {
				// set 'responded' to TRUE if is able to connect with google
				// mobile (responds fast)
				new Thread() {
					@Override
					public void run() {
						HttpGet requestForTest = new HttpGet(
								"http://m.google.com");
						try {
							new DefaultHttpClient().execute(requestForTest); // can
																				// last...
							responded = true;
						} catch (Exception e) {
						}
					}
				}.start();

				try {
					int waited = 0;
					while (!responded && (waited < timeout)) {
						sleep(100);
						if (!responded) {
							waited += 100;
						}
					}
				} catch (InterruptedException e) {
				} // do nothing
				finally {
					if (!responded) {
						handler.sendEmptyMessage(0);
					} else {
						handler.sendEmptyMessage(1);
					}
				}
			}
		}.start();
	}

	/* Reading twitter essential configuration parameters from strings.xml */
	private void initTwitterConfigs() {
		consumerKey = getString(R.string.twitter_consumer_key);
		consumerSecret = getString(R.string.twitter_consumer_secret);
		callbackUrl = getString(R.string.twitter_callback);
		oAuthVerifier = getString(R.string.twitter_oauth_verifier);
	}

	private void loginToTwitter() {
		boolean isLoggedIn = mSharedPreferences.getBoolean(
				PREF_KEY_TWITTER_LOGIN, false);

		if (!isLoggedIn) {
			Log.d("!isLoggedIn----", "!isLoggedIn");
			Log.d("!consumerKey----", "" + consumerKey);
			Log.d("!consumerSecret----", "" + consumerSecret);
			final ConfigurationBuilder builder = new ConfigurationBuilder();
			builder.setOAuthConsumerKey(consumerKey);
			builder.setOAuthConsumerSecret(consumerSecret);

			final Configuration configuration = builder.build();
			final TwitterFactory factory = new TwitterFactory(configuration);
			twitter = factory.getInstance();

			try {
				requestToken = twitter.getOAuthRequestToken(callbackUrl);
				Log.d("!callbackUrl----", "" + callbackUrl);
				Log.d("!requestToken----", "" + requestToken);
				/**
				 * Loading twitter login page on webview for authorization Once
				 * authorized, results are received at onActivityResult
				 * */
				final Intent intent = new Intent(this, WebViewActivity.class);
				intent.putExtra(WebViewActivity.EXTRA_URL,
						requestToken.getAuthenticationURL());
				startActivityForResult(intent, WEBVIEW_REQUEST_CODE);
				Log.d("!consumerSecret----", "calling");
			} catch (TwitterException e) {
				e.printStackTrace();
			}
		} else {
			Log.d("--loginToTwitter--", "!loginToTwitter");
			new updateTwitterStatus()
					.execute("http://mfvs.cc/buddha/uploads/419498371_807904775.png");
		}
	}

	class updateTwitterStatus extends AsyncTask<String, String, Void> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			mDialogTwitter = new ProgressDialog(DownloadListActivity.this);
			mDialogTwitter.setMessage("Posting to twitter...");
			mDialogTwitter.setIndeterminate(false);
			mDialogTwitter.setCancelable(false);
			mDialogTwitter.show();
		}

		protected Void doInBackground(String... args) {

			String status = args[0];
			try {
				ConfigurationBuilder builder = new ConfigurationBuilder();
				builder.setOAuthConsumerKey(consumerKey);
				builder.setOAuthConsumerSecret(consumerSecret);

				// Access Token
				String access_token = mSharedPreferences.getString(
						PREF_KEY_OAUTH_TOKEN, "");
				// Access Token Secret
				String access_token_secret = mSharedPreferences.getString(
						PREF_KEY_OAUTH_SECRET, "");

				AccessToken accessToken = new AccessToken(access_token,
						access_token_secret);
				Twitter twitter = new TwitterFactory(builder.build())
						.getInstance(accessToken);

				// Update status
				StatusUpdate statusUpdate = new StatusUpdate(status);
				// InputStream is =
				// getResources().openRawResource(R.drawable.lakeside_view);
				// statusUpdate.setMedia("test.jpg", is);

				twitter4j.Status response = twitter.updateStatus(statusUpdate);

				Log.d("Status", response.getText());

			} catch (TwitterException e) {
				Log.d("Failed to post!", e.getMessage());
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {

			/* Dismiss the progress dialog after sharing */
			mDialogTwitter.dismiss();

			Toast.makeText(DownloadListActivity.this, "Posted to Twitter!",
					Toast.LENGTH_SHORT).show();

		}

	}

	/**
	 * Activity result for twitter
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode == Activity.RESULT_OK) {
			String verifier = data.getExtras().getString(oAuthVerifier);
			try {
				Log.d("onACtivityResult----", "onACtivityResult");
				AccessToken accessToken = twitter.getOAuthAccessToken(
						requestToken, verifier);

				long userID = accessToken.getUserId();
				final User user = twitter.showUser(userID);
				@SuppressWarnings("unused")
				String username = user.getName();

				saveTwitterInfo(accessToken);

				Log.d("--onACtivityResult--", "!onACtivityResult");
				new updateTwitterStatus()
						.execute("http://mfvs.cc/buddha/uploads/419498371_807904775.png");
			} catch (Exception e) {
				// e.getMessage();
				// Log.d("Twitter Login Failed", e.getMessage());
			}
		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	/**
	 * Saving user information, after user is authenticated for the first time.
	 * You don't need to show user to login, until user has a valid access toen
	 */
	private void saveTwitterInfo(AccessToken accessToken) {

		long userID = accessToken.getUserId();
		Log.d("userID----", "userID");

		User user;
		try {
			user = twitter.showUser(userID);

			String username = user.getName();

			/* Storing oAuth tokens to shared preferences */
			Editor e = mSharedPreferences.edit();
			e.putString(PREF_KEY_OAUTH_TOKEN, accessToken.getToken());
			e.putString(PREF_KEY_OAUTH_SECRET, accessToken.getTokenSecret());
			e.putBoolean(PREF_KEY_TWITTER_LOGIN, true);
			e.putString(PREF_USER_NAME, username);
			e.commit();

		} catch (TwitterException e1) {
			e1.printStackTrace();
		}
	}

	private void initView() {
		// TODO Auto-generated method stub
		backLinear = (RelativeLayout) findViewById(R.id.backLinear);
		backLinear.setOnClickListener(this);
		photographLinear = (LinearLayout) findViewById(R.id.photographLinear);
		photographLinear.setOnClickListener(this);
		videoLinear = (LinearLayout) findViewById(R.id.videoLinear);
		videoLinear.setOnClickListener(this);
		photographText = (TextView) findViewById(R.id.photographText);
		videoText = (TextView) findViewById(R.id.videoText);

		View downloadingView = LayoutInflater.from(mContext).inflate(
				R.layout.downloading_view, null);
		views.add(downloadingView);
		View downloadedView = LayoutInflater.from(mContext).inflate(
				R.layout.downloaded_view, null);

		views.add(downloadedView);
		viewPager = (ViewPager) findViewById(R.id.viewPager);
		viewPager.setAdapter(new ViewPagerAdapter(views));
		viewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub
				if (arg0 == 0) {
					photographLinear.setBackgroundResource(R.drawable.shadow);
					photographText.setTextColor(getResources().getColor(
							R.color.white));
					videoLinear.setBackgroundColor(Color.TRANSPARENT);
					videoText.setTextColor(getResources().getColor(
							R.color.line_color));
				} else {
					photographLinear.setBackgroundColor(Color.TRANSPARENT);
					photographText.setTextColor(getResources().getColor(
							R.color.line_color));
					videoLinear.setBackgroundResource(R.drawable.shadow);
					videoText.setTextColor(getResources().getColor(
							R.color.white));
					Foruploading.setVisibility(View.VISIBLE);
				}
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}
		});

		downloading = (ListView) downloadingView.findViewById(R.id.downloading);
		downloadingAdapter = new DownloadListAdapter(mContext);
		downloading.setAdapter(downloadingAdapter);
		downloaded = (ListView) downloadedView.findViewById(R.id.downloaded);
		if (downloadManager.getDownloadedInfo() != null) {
			downloadedInfoList.addAll(downloadManager.getDownloadedInfo());
		}
		downloadedAdapter = new DownloadedAdapter(downloadedInfoList);
		Log.e("downloadedInfoList", "" + downloadedInfoList.toString());
		downloaded.setAdapter(downloadedAdapter);
		downloaded.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				DownloadInfo downloadInfo = (DownloadInfo) arg0
						.getItemAtPosition(arg2);
				ArrayList<String> list = new ArrayList<String>();
				if (downloadInfo.getDownloadUrl().contains("JPG")) {
					int index = 0;
					for (int i = 0; i < downloadedInfoList.size(); i++) {
						if (downloadedInfoList.get(i).getDownloadUrl()
								.contains("JPG")) {
							list.add(downloadedInfoList.get(i)
									.getFileSavePath());
						}
						if (downloadInfo.getFileName().equals(
								downloadedInfoList.get(i).getFileName())) {
							index = i;
						}
					}
					Intent intent = new Intent(mContext,
							PlayBackDetailActivity.class);
					intent.putExtra("list", list);
					intent.putExtra("index", index);
					intent.putExtra("photoName", downloadInfo.getDownloadUrl());
					startActivity(intent);

				} else {
					Intent intent = new Intent(Intent.ACTION_VIEW);
					intent.setDataAndType(
							Uri.parse(downloadInfo.getFileSavePath()),
							"video/mp4");
					startActivity(intent);
				}

			}
		});
	}

	public boolean checkPermissions() {
		Session s = Session.getActiveSession();
		if (s != null) {
			return s.getPermissions().contains("publish_actions");
		} else
			return false;
	}

	public void requestPermissions() {
		Session s = Session.getActiveSession();
		if (s != null)
			s.requestNewPublishPermissions(new Session.NewPermissionsRequest(
					this, PERMISSIONS));
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		uiHelper.onResume();
		downloadingAdapter.notifyDataSetChanged();
		AndroidAuthSession session = mApi.getSession();
		if (session.authenticationSuccessful()) {
			try {
				// Mandatory call to complete the auth
				session.finishAuthentication();

				// Store it locally in our app for later use
				storeAuth(session);
				setLoggedIn(true);
			} catch (IllegalStateException e) {
				showToast("Couldn't authenticate with Dropbox:"
						+ e.getLocalizedMessage());
				Log.i("Setting Activity", "Error authenticating", e);
			}
		}
	}

	private void logOut() {
		// Remove credentials from the session
		mApi.getSession().unlink();

		// Clear our stored keys
		clearKeys();
		// Change UI state to display logged out version
		setLoggedIn(false);
	}

	/**
	 * Convenience function to change UI state based on being logged in
	 */
	private void setLoggedIn(boolean loggedIn) {
		mLoggedIn = loggedIn;
		if (loggedIn) {

		} else {

		}
	}

	private void checkAppKeySetup() {
		// Check to make sure that we have a valid app key
		// if (APP_KEY.startsWith("CHANGE") ||
		// APP_SECRET.startsWith("CHANGE")) {
		// showToast("You must apply for an app key and secret from developers.dropbox.com, and add them to the DBRoulette ap before trying it.");
		// finish();
		// return;
		// }

		// Check if the app has set up its manifest properly.
		Intent testIntent = new Intent(Intent.ACTION_VIEW);
		String scheme = "db-" + APP_KEY;
		String uri = scheme + "://" + AuthActivity.AUTH_VERSION + "/test";
		testIntent.setData(Uri.parse(uri));
		PackageManager pm = getPackageManager();
		if (0 == pm.queryIntentActivities(testIntent, 0).size()) {
			showToast("URL scheme in your app's "
					+ "manifest is not set up correctly. You should have a "
					+ "com.dropbox.client2.android.AuthActivity with the "
					+ "scheme: " + scheme);
			finish();
		}
	}

	private void showToast(String msg) {
		Toast error = Toast.makeText(this, msg, Toast.LENGTH_LONG);
		error.show();
	}

	/**
	 * Shows keeping the access keys returned from Trusted Authenticator in a
	 * local store, rather than storing user name & password, and
	 * re-authenticating each time (which is not to be done, ever).
	 */
	private void loadAuth(AndroidAuthSession session) {
		SharedPreferences prefs = getSharedPreferences(ACCOUNT_PREFS_NAME, 0);
		String key = prefs.getString(ACCESS_KEY_NAME, null);
		String secret = prefs.getString(ACCESS_SECRET_NAME, null);
		if (key == null || secret == null || key.length() == 0
				|| secret.length() == 0)
			return;

		if (key.equals("oauth2:")) {
			// If the key is set to "oauth2:", then we can assume the token is
			// for OAuth 2.
			session.setOAuth2AccessToken(secret);
		} else {
			// Still support using old OAuth 1 tokens.
			session.setAccessTokenPair(new AccessTokenPair(key, secret));
		}
	}

	/**
	 * Shows keeping the access keys returned from Trusted Authenticator in a
	 * local store, rather than storing user name & password, and
	 * re-authenticating each time (which is not to be done, ever).
	 */
	private void storeAuth(AndroidAuthSession session) {
		// Store the OAuth 2 access token, if there is one.
		String oauth2AccessToken = session.getOAuth2AccessToken();
		if (oauth2AccessToken != null) {
			SharedPreferences prefs = getSharedPreferences(ACCOUNT_PREFS_NAME,
					0);
			Editor edit = prefs.edit();
			edit.putString(ACCESS_KEY_NAME, "oauth2:");
			edit.putString(ACCESS_SECRET_NAME, oauth2AccessToken);
			edit.commit();
			return;
		}
		// Store the OAuth 1 access token, if there is one. This is only
		// necessary if
		// you're still using OAuth 1.
		AccessTokenPair oauth1AccessToken = session.getAccessTokenPair();
		if (oauth1AccessToken != null) {
			SharedPreferences prefs = getSharedPreferences(ACCOUNT_PREFS_NAME,
					0);
			Editor edit = prefs.edit();
			edit.putString(ACCESS_KEY_NAME, oauth1AccessToken.key);
			edit.putString(ACCESS_SECRET_NAME, oauth1AccessToken.secret);
			edit.commit();
			return;
		}
	}

	private void clearKeys() {
		SharedPreferences prefs = getSharedPreferences(ACCOUNT_PREFS_NAME, 0);
		Editor edit = prefs.edit();
		edit.clear();
		edit.commit();
	}

	private AndroidAuthSession buildSession() {
		AppKeyPair appKeyPair = new AppKeyPair(APP_KEY, APP_SECRET);

		AndroidAuthSession session = new AndroidAuthSession(appKeyPair);
		loadAuth(session);
		return session;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putBoolean(PENDING_PUBLISH_KEY, pendingPublishReauthorization);
		uiHelper.onSaveInstanceState(outState);
	}

	@Override
	public void onPause() {
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		uiHelper.onDestroy();
		try {
			if (downloadingAdapter != null && downloadManager != null) {
				downloadManager.backupDownloadInfoList();
			}
		} catch (DbException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// @Override
	// public void onActivityResult(int requestCode, int resultCode, Intent
	// data) {
	// super.onActivityResult(requestCode, resultCode, data);
	// uiHelper.onActivityResult(requestCode, resultCode, data, dialogCallback);
	// }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.backLinear:
			finish();
			break;
		case R.id.photographLinear:
			photographLinear.setBackgroundResource(R.drawable.shadow);
			photographText.setTextColor(getResources().getColor(R.color.white));
			videoLinear.setBackgroundColor(Color.TRANSPARENT);
			videoText.setTextColor(getResources().getColor(R.color.line_color));
			viewPager.setCurrentItem(0);
			break;
		case R.id.videoLinear:
			photographLinear.setBackgroundColor(Color.TRANSPARENT);
			photographText.setTextColor(getResources().getColor(
					R.color.line_color));
			videoLinear.setBackgroundResource(R.drawable.shadow);
			videoText.setTextColor(getResources().getColor(R.color.white));
			viewPager.setCurrentItem(1);
			break;
		}
	}

	class DownloadListAdapter extends BaseAdapter {
		LayoutInflater mInflater;
		Context mContext;
		DownloadViewHoldler holdler = null;

		public DownloadListAdapter(Context mContext) {
			// TODO Auto-generated constructor stub
			mInflater = LayoutInflater.from(mContext);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			if (downloadManager == null)
				return 0;
			return downloadManager.getDownloadInfoListCount();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return downloadManager.getDownloadInfo(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			DownloadInfo downloadInfo = downloadManager
					.getDownloadInfo(position);
			if (convertView == null) {
				convertView = mInflater
						.inflate(R.layout.downloading_item, null);
				holdler = new DownloadViewHoldler(downloadInfo);
				ViewUtils.inject(holdler, convertView);
				convertView.setTag(holdler);
				holdler.refresh();
			} else {
				holdler = (DownloadViewHoldler) convertView.getTag();
				holdler.update(downloadInfo);
			}
			HttpHandler<File> handler = downloadInfo.getHandler();
			if (handler != null) {
				RequestCallBack callBack = handler.getRequestCallBack();
				if (callBack instanceof DownloadManager.ManagerCallBack) {
					DownloadManager.ManagerCallBack managerCallBack = (DownloadManager.ManagerCallBack) callBack;
					if (managerCallBack.getBaseCallBack() == null) {
						managerCallBack
								.setBaseCallBack(new DownloadRequestCallBack());
					}
				}
				callBack.setUserTag(new WeakReference<DownloadViewHoldler>(
						holdler));
			}
			return convertView;
		}

		public class DownloadViewHoldler {
			@ViewInject(R.id.download_progress)
			ProgressBar progressBar;
			@ViewInject(R.id.download_title)
			TextView downloadFileName;
			@ViewInject(R.id.percenttv)
			TextView percent;
			@ViewInject(R.id.download_status)
			Button downloadStatus;
			@ViewInject(R.id.download_delete)
			Button downloadDelete;
			@ViewInject(R.id.linear)
			LinearLayout linear;
			private DownloadInfo downloadInfo;

			public DownloadViewHoldler(DownloadInfo downloadInfo) {
				// TODO Auto-generated constructor stub
				this.downloadInfo = downloadInfo;
			}

			public void update(DownloadInfo downloadInfo) {
				this.downloadInfo = downloadInfo;
				refresh();
			}

			@OnClick(R.id.download_status)
			public void stop(View view) {
				HttpHandler.State state = downloadInfo.getState();
				switch (state) {
				case WAITING:
				case STARTED:
				case LOADING:
					try {
						downloadManager.stopDownload(downloadInfo);
					} catch (DbException e) {
						e.printStackTrace();
					}
					break;
				case CANCELLED:
				case FAILURE:
					try {
						downloadManager.resumeDownload(downloadInfo,
								new DownloadRequestCallBack());
						downloadingAdapter.notifyDataSetChanged();
					} catch (DbException e) {
						e.printStackTrace();
					}

					break;
				}
			}

			@OnClick(R.id.download_delete)
			public void remove(View view) {
				// TODO Auto-generated method stub
				AlertDialog.Builder builder = new AlertDialog.Builder(
						DownloadListActivity.this);
				builder.setTitle(DownloadListActivity.this
						.getString(R.string.prompt));
				builder.setMessage(DownloadListActivity.this
						.getString(R.string.delete_file));
				builder.setPositiveButton(
						DownloadListActivity.this.getString(R.string.sure),
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								dialog.dismiss();
								try {
									downloadManager
											.removeDownload(downloadInfo);
									downloadingAdapter.notifyDataSetChanged();
								} catch (DbException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						});
				builder.setNegativeButton(
						DownloadListActivity.this.getString(R.string.cancel),
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								dialog.dismiss();
							}
						});
				builder.show();
			}

			public void refresh() {
				downloadFileName.setText(downloadInfo.getFileName());
				if (downloadInfo.getFileLength() > 0) {
					int percentText = (int) (downloadInfo.getProgress() * 100 / downloadInfo
							.getFileLength());
					progressBar.setProgress(percentText);
					percent.setText(percentText + "%");
					Log.e("test", percentText + "%");
				} else {
					percent.setText("0%");
					progressBar.setProgress(0);
				}
				linear.setVisibility(View.VISIBLE);
				downloadStatus.setBackgroundResource(R.drawable.download_pause);
				HttpHandler.State state = downloadInfo.getState();
				switch (state) {
				case WAITING:
					downloadStatus
							.setBackgroundResource(R.drawable.download_pause);
					break;
				case STARTED:
					downloadStatus
							.setBackgroundResource(R.drawable.download_start);
					break;
				case LOADING:
					downloadStatus
							.setBackgroundResource(R.drawable.download_start);
					break;
				case CANCELLED:
					downloadStatus
							.setBackgroundResource(R.drawable.download_pause);
					break;
				case SUCCESS:
					linear.setVisibility(View.INVISIBLE);
					downloadManager.removeDownloadInfo(downloadInfo);
					downloadedInfoList.clear();
					downloadedInfoList.addAll(downloadManager
							.getDownloadedInfo());
					downloadedAdapter.notifyDataSetChanged();
					break;
				case FAILURE:
					downloadStatus
							.setBackgroundResource(R.drawable.download_pause);
					break;
				}
				downloadingAdapter.notifyDataSetChanged();
			}

		}
	}

	private class DownloadRequestCallBack extends RequestCallBack<File> {

		@SuppressWarnings("unchecked")
		private void refreshListItem() {
			if (userTag == null)
				return;
			WeakReference<DownloadViewHoldler> tag = (WeakReference<DownloadViewHoldler>) userTag;
			DownloadViewHoldler holder = tag.get();
			if (holder != null) {
				holder.refresh();
			}
		}

		@Override
		public void onStart() {
			refreshListItem();
		}

		@Override
		public void onLoading(long total, long current, boolean isUploading) {
			refreshListItem();
		}

		@Override
		public void onSuccess(ResponseInfo<File> responseInfo) {
			refreshListItem();
		}

		@Override
		public void onFailure(HttpException error, String msg) {
			refreshListItem();
		}

		@Override
		public void onCancelled() {
			// TODO Auto-generated method stub
			refreshListItem();
		}
	}

	class DownloadedAdapter extends BaseAdapter {
		private List<DownloadInfo> list;
		ArrayList<String> path = new ArrayList<String>();

		public DownloadedAdapter(List<DownloadInfo> list) {
			// TODO Auto-generated constructor stub
			this.list = list;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size() > 0 ? list.size() : 0;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub

			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			final ViewHolder holder;
			Bitmap bitmap = null;
			BitmapFactory.Options options;
			if (convertView == null) {
				convertView = LayoutInflater.from(mContext).inflate(
						R.layout.downloaded_item, null);
				holder = new ViewHolder();
				holder.fileName = (TextView) convertView
						.findViewById(R.id.download_title);
				holder.delete = (Button) convertView
						.findViewById(R.id.download_delete);
				holder.fbShare = (Button) convertView
						.findViewById(R.id.shareButton);
				holder.icon = (ImageView) convertView
						.findViewById(R.id.downloadedview);
				holder.icon1 = (ImageView) convertView
						.findViewById(R.id.Videothumb1);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			final DownloadInfo downloadInfo = list.get(position);
			Log.e("adapter", "" + downloadInfo);
			holder.fileName.setText(downloadInfo.getFileName());
			Log.e("AbsolutePath", "" + downloadInfo.getFileSavePath());
			path.add(downloadInfo.getFileSavePath());
			if (downloadInfo.getFileName().contains(".JPG")) {
				options = new BitmapFactory.Options();
				options.inSampleSize = 8;
				bitmap = BitmapFactory.decodeFile(path.get(position), options);

			} else if (downloadInfo.getFileName().contains(".mp4")) {

				bitmap = ThumbnailUtils.createVideoThumbnail(
						path.get(position), position);
				holder.icon1.setVisibility(View.VISIBLE);
			}

			holder.icon.setImageBitmap(bitmap);
			holder.delete.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					AlertDialog.Builder builder = new AlertDialog.Builder(
							DownloadListActivity.this);
					builder.setTitle(DownloadListActivity.this
							.getString(R.string.prompt));
					builder.setMessage(DownloadListActivity.this
							.getString(R.string.delete_file));
					builder.setPositiveButton(
							DownloadListActivity.this.getString(R.string.sure),
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
									dialog.dismiss();
									try {
										downloadManager
												.removeDownload(downloadInfo);
										downloadedInfoList.clear();
										downloadedInfoList
												.addAll(downloadManager
														.getDownloadedInfo());
										notifyDataSetChanged();
									} catch (DbException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
							});
					builder.setNegativeButton(DownloadListActivity.this
							.getString(R.string.cancel),
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
									dialog.dismiss();
								}
							});
					builder.show();
				}
			});

			holder.fbShare.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Log.e("downloadInfo.getFileSavePath(): ",
							"" + downloadInfo.getFileSavePath());
					Log.e("downloadInfo.getDownloadUrl(): ",
							"" + downloadInfo.getDownloadUrl());
					Log.e("downloadInfo.getFileName(): ",
							"" + downloadInfo.getFileName());
					if (downloadInfo.getFileName().endsWith(".mp4")) {
						Log.e("", "Onlu MP4");
						shareImageusingChooser(downloadInfo.getFileSavePath());

					} else {

						Log.e("", "Onlu image");
						shareImageChooser(downloadInfo.getFileSavePath());
					}

					// shareVideo(downloadInfo.getFileSavePath());
					// ShareDialog(downloadInfo.getFileSavePath());

				}
			});

			return convertView;
		}

		class ViewHolder {
			Button delete, fbShare;
			TextView fileName;
			ImageView icon, icon1;
		}

	}

	public void shareImageusingChooser(String mString) {
		String imagePath = mString;
		File imageFileToShare = new File(imagePath);
		Uri uri = Uri.fromFile(imageFileToShare);
		// Set the action to be performed i.e 'Send Data'
		Intent sendIntent = new Intent(Intent.ACTION_SEND);
		// Add the URI holding a stream of data
		sendIntent.putExtra(Intent.EXTRA_STREAM, uri);
		// Set the type of data i.e 'image/* which means image/png, image/jpg,
		// image/jpeg etc.,'
		sendIntent.setType("video/*");
		// Shows chooser (List of Apps) that can handle Image; You have to
		// choose one among them
		startActivity(Intent.createChooser(sendIntent, "SHARE"));
	}

	public void shareImageChooser(String mString) {
		String imagePath = mString;
		File imageFileToShare = new File(imagePath);
		Uri uri = Uri.fromFile(imageFileToShare);
		// Set the action to be performed i.e 'Send Data'
		Intent sendIntent = new Intent(Intent.ACTION_SEND);
		// Add the URI holding a stream of data
		sendIntent.putExtra(Intent.EXTRA_STREAM, uri);
		// Set the type of data i.e 'image/* which means image/png, image/jpg,
		// image/jpeg etc.,'
		sendIntent.setType("image/*");
		// Shows chooser (List of Apps) that can handle Image; You have to
		// choose one among them
		startActivity(Intent.createChooser(sendIntent, "SHARE"));
	}

	public void sharefacebook(String path) {
		//
		// if (checkPermissions()) {
		// Request request = Request.newStatusUpdateRequest(
		// Session.getActiveSession(), "Message",
		// new Request.Callback() {
		// @Override
		// public void onCompleted(Response response) {
		// if (response.getError() == null)
		// Toast.makeText(DownloadListActivity.this,
		// "Status updated successfully",
		// Toast.LENGTH_LONG).show();
		// }
		// });
		// request.executeAsync();
		// } else {
		// requestPermissions();
		// }

		// // Create a new file for the video
		// File file = new File(path);
		// try {
		// // create a new request to upload video to the facebook
		// Request videoRequest = Request.newUploadVideoRequest(session,
		// file, new Request.Callback() {
		//
		// @Override
		// public void onCompleted(Response response) {
		//
		// if (response.getError() == null) {
		// Toast.makeText(DownloadListActivity.this,
		// "video shared successfully",
		// Toast.LENGTH_SHORT).show();
		// } else {
		// Toast.makeText(
		// DownloadListActivity.this,
		// response.getError()
		// .getErrorMessage(),
		// Toast.LENGTH_SHORT).show();
		// }
		// }
		// });
		//
		// // Execute the request in a separate thread
		// videoRequest.executeAsync();
		//
		// } catch (FileNotFoundException e) {
		// e.printStackTrace();
		// }
		// } else {
		// Toast.makeText(getApplicationContext(),
		// "Please login to facebook first", Toast.LENGTH_SHORT)
		// .show();
		// }

		Session session = Session.getActiveSession();
		Log.e("SESSION:", "" + session);

		if (session != null) {

			if (FacebookDialog.canPresentShareDialog(DownloadListActivity.this,
					FacebookDialog.ShareDialogFeature.SHARE_DIALOG)) {
				Log.e("path:", "" + path);
				FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(
						DownloadListActivity.this).setLink(path).build();
				uiHelper.trackPendingDialogCall(shareDialog.present());

			} else {
				// publishFeedDialog();
			}

			// }

		}
	}

	/**
	 * Show custom dialog
	 */
	private void ShareDialog(final String path) {
		// Create custom dialog object
		final Dialog dialog = new Dialog(DownloadListActivity.this);
		// hide to default title for Dialog
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		// inflate the layout dialog_layout.xml and set it as contentView
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.custom_dialog, null, false);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setContentView(view);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));

		ImageView mLayout_facebook = (ImageView) dialog
				.findViewById(R.id.facebook_image);
		ImageView mLayout_twitter = (ImageView) dialog
				.findViewById(R.id.twitter_image);

		ImageView mLayout_intagram = (ImageView) dialog
				.findViewById(R.id.instgram_image);
		pinIt = (PinItButton) dialog.findViewById(R.id.pinterest_image);
		pinIt.setImageResource(R.drawable.pinterest_share);
		pinIt.setUrl("http://mfvs.cc/buddha/uploads/419498371_807904775.png");

		pinIt.setDescription("ClickPro");
		mLayout_intagram.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.dismiss();
			}
		});
		mLayout_facebook.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				// Dismiss the dialog
				sharefacebook(path);
				dialog.dismiss();
			}
		});
		mLayout_twitter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				loginToTwitter();
				// Dismiss the dialog
				dialog.dismiss();
			}
		});
		pinIt.setListener(new PinItListener() {
			@Override
			public void onStart() {
				super.onStart();
			}

			@Override
			public void onComplete(boolean completed) {
				super.onComplete(completed);
				if (completed) {
					System.out.println("Pinit complete");
					dialog.dismiss();
				}
			}

			@Override
			public void onException(Exception e) {
				super.onException(e);
				System.out.println("Pinit Exception");
			}
		});

		// pinIt.setOnClickListener(new OnClickListener() {
		// @Override
		// public void onClick(View v) {
		//
		// // Dismiss the dialog
		// dialog.dismiss();
		// }
		// });

		Button btnCancel = (Button) dialog
				.findViewById(R.id.btn_show_custom_dialog);
		btnCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close the dialog
				dialog.dismiss();
			}
		});

		// Display the dialog
		dialog.show();
	}

	private boolean isSubsetOf(Collection<String> subset,
			Collection<String> superset) {
		for (String string : subset) {
			if (!superset.contains(string)) {
				return false;
			}
		}
		return true;
	}

}
