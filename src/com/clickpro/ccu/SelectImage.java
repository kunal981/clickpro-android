package com.clickpro.ccu;

import java.io.File;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.ImageView;

public class SelectImage extends FragmentActivity {

	ImageView view;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.selectimage);
		view = (ImageView) findViewById(R.id.Selected);

		Intent i = getIntent();
		String position = i.getExtras().getString("Index");
		Log.v("", "" + position);
		File imgFile = new File(position);
		if (imgFile.exists()) {

			Bitmap myBitmap = BitmapFactory.decodeFile(imgFile
					.getAbsolutePath());
			view.setImageBitmap(myBitmap);
		}

	}

	public void onBackPressed() {
		finish();

	}
}
