package com.clickpro.ccu;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.clickpro.adapter.PlayBackAdapter;
import com.clickpro.adapter.VideoAdapter;
import com.clickpro.adapter.ViewPagerAdapter;
import com.clickpro.view.CustomDialog;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;

/**
 * �طŽ���
 * 
 * @description
 * @version 1.0
 * @update 2013-10-10 ����7:49:08
 */
public class PlayBackActivity extends BaseActivity implements OnClickListener {
	private ListView photoGridView, videoGridview;
	private PlayBackAdapter adapter;
	private VideoAdapter videoAdapter;
	private Context context;
	private RelativeLayout backLinear;
	private LinearLayout photographLinear, videoLinear;
	private TextView photographText, videoText;
	private ArrayList<String> photos = new ArrayList<String>();
	private ArrayList<String> videos = new ArrayList<String>();
	private ViewPager viewPager;
	private List<View> views = new ArrayList<View>();
	private Button localResource;
	private String SDPath = Environment.getExternalStorageDirectory()
			+ File.separator + Environment.DIRECTORY_DOWNLOADS + File.separator;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.play_back);
		context = this;
		initView();
		new GetPhotoAndVideo().execute();
		if (!new File(SDPath).exists()) {
			new File(SDPath).mkdirs();
		}
	}

	private void initView() {
		// TODO Auto-generated method stub
		backLinear = (RelativeLayout) findViewById(R.id.backLinear);
		backLinear.setOnClickListener(this);
		photographLinear = (LinearLayout) findViewById(R.id.photographLinear);
		photographLinear.setOnClickListener(this);
		videoLinear = (LinearLayout) findViewById(R.id.videoLinear);
		videoLinear.setOnClickListener(this);
		photographText = (TextView) findViewById(R.id.photographText);
		videoText = (TextView) findViewById(R.id.videoText);
		localResource = (Button) findViewById(R.id.localResource);
		localResource.setOnClickListener(this);

		View photoView = LayoutInflater.from(context).inflate(
				R.layout.play_back_photo_view, null);
		views.add(photoView);
		View videoView = LayoutInflater.from(context).inflate(
				R.layout.play_back_video_view, null);
		views.add(videoView);
		viewPager = (ViewPager) findViewById(R.id.viewPager);
		viewPager.setAdapter(new ViewPagerAdapter(views));
		viewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub
				if (arg0 == 0) {
					photographLinear.setBackgroundResource(R.drawable.shadow);
					photographText.setTextColor(getResources().getColor(
							R.color.white));
					videoLinear.setBackgroundColor(Color.TRANSPARENT);
					videoText.setTextColor(getResources().getColor(
							R.color.line_color));
				} else {
					photographLinear.setBackgroundColor(Color.TRANSPARENT);
					photographText.setTextColor(getResources().getColor(
							R.color.line_color));
					videoLinear.setBackgroundResource(R.drawable.shadow);
					videoText.setTextColor(getResources().getColor(
							R.color.white));
				}
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}
		});

		photoGridView = (ListView) photoView.findViewById(R.id.photoListView);
		videoGridview = (ListView) videoView.findViewById(R.id.videoListView);
		videoAdapter = new VideoAdapter(context, videos);
		videoGridview.setAdapter(videoAdapter);
		adapter = new PlayBackAdapter(context, photos);
		photoGridView.setAdapter(adapter);
		photoGridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				String url = (String) arg0.getItemAtPosition(arg2);
				Log.e("url: ", "" + url);
				Intent intent = new Intent(PlayBackActivity.this,
						PlayBackDetailActivity.class);
				intent.putExtra("list", photos);
				intent.putExtra("index", 0);
				intent.putExtra("photoName", url);
				startActivity(intent);

			}
		});
		videoGridview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				String videoPath = (String) arg0.getItemAtPosition(arg2);
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setDataAndType(Uri.parse(videoPath), "video/mp4");
				startActivity(intent);
				// String url = (String) arg0.getItemAtPosition(arg2);
				// Intent intent = new Intent(PlayBackActivity.this,
				// PlayerActivity.class);
				// intent.putExtra("list", videos);
				// intent.putExtra("index", arg2);
				// intent.putExtra("url", url);
				// startActivity(intent);
			}
		});

	}

	class GetPhotoAndVideo extends AsyncTask<String, String, String> {
		CustomDialog dialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = new CustomDialog(context);
			dialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			Log.e("doInBackground: ", "doInBackground");
			// String path = "http://192.168.42.1/DCIM/100MEDIA/?order=N";
			String path = "http://192.168.42.1/DCIM/100MEDIA/";
			String url = "http://192.168.42.1/DCIM/100MEDIA/";
			try {
				Document doc = Jsoup.connect(path).get();
				Log.e("doc: ", "" + doc);
				Elements links = doc.getElementsByClass("link");
				Log.e("links: ", "" + links);
				for (Element link : links) {
					String fileName = link.text();
					if (fileName.split("\\.")[1].equals("mp4")) {
						videos.add(url + fileName);
					} else {
						photos.add(url + fileName);
					}

				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			dialog.dismiss();
			if (adapter != null && videoAdapter != null) {
				adapter.notifyDataSetChanged();
				videoAdapter.notifyDataSetChanged();
			}
		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.backLinear:
			finish();
			break;
		case R.id.photographLinear:
			photographLinear.setBackgroundResource(R.drawable.shadow);
			photographText.setTextColor(getResources().getColor(R.color.white));
			videoLinear.setBackgroundColor(Color.TRANSPARENT);
			videoText.setTextColor(getResources().getColor(R.color.line_color));
			viewPager.setCurrentItem(0);
			break;
		case R.id.videoLinear:
			photographLinear.setBackgroundColor(Color.TRANSPARENT);
			photographText.setTextColor(getResources().getColor(
					R.color.line_color));
			videoLinear.setBackgroundResource(R.drawable.shadow);
			videoText.setTextColor(getResources().getColor(R.color.white));
			viewPager.setCurrentItem(1);
			break;
		case R.id.localResource:
			Intent intent = new Intent(context, DownloadListActivity.class);
			startActivity(intent);
			break;
		default:
			break;
		}
	}
}
