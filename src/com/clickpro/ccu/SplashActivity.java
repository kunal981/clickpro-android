/**
 * 
 */
package com.clickpro.ccu;

import java.util.ArrayList;
import java.util.List;

import com.clickpro.util.MySharedPreferences;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;

/**
 * ����ҳ
 * 
 * @description
 * @version 1.0
 * @author ������
 * @update 2013-11-9 ����12:23:29
 */

public class SplashActivity extends BaseActivity {
	private ViewPager viewpager;
	private LinearLayout point;
	private List<View> list = new ArrayList<View>();
	private Context context;
	private View view1, view2, view3;
	private List<ImageView> imageViews;
	private ImageView start;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		context = this;
		viewpager = (ViewPager) findViewById(R.id.viewpager);
		viewpager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub
				draw_Point(arg0);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}
		});
		point = (LinearLayout) findViewById(R.id.point);
		view1 = LayoutInflater.from(context).inflate(R.layout.view1, null);
		view2 = LayoutInflater.from(context).inflate(R.layout.view2, null);
		view3 = LayoutInflater.from(context).inflate(R.layout.view3, null);
		start = (ImageView) view3.findViewById(R.id.start);
		start.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String first = MySharedPreferences.getStringValue(context,
						MySharedPreferences.isFirst);
				if (first != null && !first.equals("")) {
					finish();
				} else {
					MySharedPreferences.setString(context,
							MySharedPreferences.isFirst, "2");
					Intent intent = new Intent(context, ChoiceActivity.class);
					startActivity(intent);
					finish();
				}

			}
		});
		list.add(view1);
		list.add(view2);
		list.add(view3);
		viewpager.setAdapter(new ImageAdapter(list));
		initPoint();
		draw_Point(0);

	}

	private void initPoint() {
		imageViews = new ArrayList<ImageView>();
		ImageView imageView;

		for (int i = 0; i < list.size(); i++) {
			imageView = new ImageView(context);
			imageView.setBackgroundResource(R.drawable.white_point);
			LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
					new ViewGroup.LayoutParams(LayoutParams.WRAP_CONTENT,
							LayoutParams.WRAP_CONTENT));
			layoutParams.leftMargin = 15;
			point.addView(imageView, layoutParams);
			imageViews.add(imageView);
		}

	}

	/***
	 * ����ѡ�е�
	 * 
	 * @param index
	 */
	private void draw_Point(int index) {
		for (int i = 0; i < imageViews.size(); i++) {
			imageViews.get(i).setImageResource(R.drawable.white_point);
		}
		imageViews.get(index).setImageResource(R.drawable.red_point);

	}

	class ImageAdapter extends PagerAdapter {
		private List<View> views;

		public ImageAdapter(List<View> views) {
			this.views = views;
		}

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {
			// TODO Auto-generated method stub
			((ViewPager) arg0).removeView(views.get(arg1));
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return views.size();
		}

		@Override
		public Object instantiateItem(View arg0, int arg1) {
			// TODO Auto-generated method stub
			((ViewPager) arg0).addView(views.get(arg1), 0);
			return views.get(arg1);
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			// TODO Auto-generated method stub
			return arg0 == arg1;
		}

	}
}
