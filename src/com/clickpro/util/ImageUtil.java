/**
 * 
 */
package com.clickpro.util;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.util.DisplayMetrics;

/**
 * �ȱ�������ͼƬ
 * 
 * @description
 * @version 1.0
 * @update 2013-10-11 ����4:16:54
 */

public class ImageUtil {
	/**
	 * (����)�ػ�ͼƬ
	 * 
	 * @param context
	 *            Activity
	 * @param bitmap
	 * @return
	 */
	public static Bitmap reDrawBitMap(Activity activity, Bitmap bitmap) {
		DisplayMetrics dm = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
		// int rHeight = dm.heightPixels;
		int rWidth = dm.widthPixels;
		// int height=bitmap.getHeight();
		int width = bitmap.getWidth();
		float zoomScale = ((float) rWidth) / width;
		Matrix matrix = new Matrix();
		// ����ͼƬ����
		matrix.postScale(zoomScale, zoomScale);
		Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0,
				bitmap.getWidth(), bitmap.getHeight(), matrix, true);
		return resizedBitmap;
	}
}
