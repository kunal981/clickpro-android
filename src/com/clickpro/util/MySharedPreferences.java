package com.clickpro.util;

import android.content.Context;
import android.content.SharedPreferences;

public class MySharedPreferences {
	private static SharedPreferences share;
	/** �Ƿ��ǵ�һ������ **/
	public static final String isFirst = "2";

	public static SharedPreferences getSharedPref(Context context) {
		share = context.getApplicationContext()
				.getSharedPreferences("rd990", 0);
		return share;
	}

	public static String getStringValue(Context context, String key) {
		if (share == null) {
			getSharedPref(context);
		}
		return share.getString(key, null);
	}

	public static void setString(Context context, String key, String value) {
		if (share == null) {
			getSharedPref(context);
		}
		share.edit().putString(key, value).commit();
	}

	/** ������ش洢���� **/
	public static void clearData(Context context) {
		if (share == null) {
			getSharedPref(context);
		}
		share.edit().clear().commit();
	}
}
