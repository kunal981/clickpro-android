package com.clickpro.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.Environment;
import android.os.Handler;

/***
 * ����ͼƬ
 * 
 * @description
 * @version 1.0
 * @author ������
 * @update 2013-12-18 ����4:43:01
 */
public class SaveImages {
	static String filePath = Environment.getExternalStorageDirectory()
			+ "/RD990"; // ��ȡ�ⲿ�豸·��
	static boolean isSuccess = false;

	public static void storeInSD(final Bitmap bitmap, final String imagePath,
			Handler mHandler) {
		new Thread(new Runnable() {
			public void run() {
				try {
					if (Environment.getExternalStorageState().equals(
							android.os.Environment.MEDIA_MOUNTED)) {// �ж��Ƿ����SD��
						File file = new File(filePath);
						if (!file.exists()) {
							file.mkdirs();
						}
						try {
							FileOutputStream fos = new FileOutputStream(
									filePath + "/" + imagePath + ".JPG");
							bitmap.compress(CompressFormat.PNG, 100, fos);
							fos.flush();
							fos.close();
							isSuccess = true;
						} catch (FileNotFoundException e) {
							e.printStackTrace();
							isSuccess = false;
						} catch (IOException e) {
							e.printStackTrace();
							isSuccess = false;
						}
					}
				} catch (Exception e) {
					// TODO: handle exception
					isSuccess = false;
				}
			}
		}).start();

		if (isSuccess) {
			mHandler.obtainMessage(0, isSuccess).sendToTarget();
		}
	}
}
