/**
 * 
 */
package com.clickpro.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.clickpro.ccu.R;

/**
 * @description
 * @version 1.0
 * @author ������
 * @update 2013-11-7 ����3:08:21
 */

public class CommonUtil {
	/**
	 * �ж��ֻ������Ƿ����
	 * 
	 * @param context
	 * @return
	 */
	public static boolean isNetworkAvailable(Context ctx) {
		ConnectivityManager mgr = (ConnectivityManager) ctx
				.getApplicationContext().getSystemService(
						Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] info = mgr.getAllNetworkInfo();
		if (info != null) {
			for (int i = 0; i < info.length; i++) {
				if (info[i].getState() == NetworkInfo.State.CONNECTED) {
					return true;
				}
			}
		}
		return false;
	}

	/***
	 * ��ʾtoast
	 * 
	 * @param context
	 * @param message
	 * @description
	 * @version 1.0
	 * @author ������
	 * @update 2013-11-8 ����1:51:00
	 */
	public static void showToast(Context context, String message) {
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}

	/**
	 * ��ʾ�Զ���toast
	 * 
	 * @param context
	 * @description ��һ�仰˵�����������ʲô
	 * @version 1.0
	 * @author ������
	 * @update 2013-11-8 ����6:34:48
	 */
	public static void showCustomToast(Context context) {
		Toast toast = new Toast(context);
		toast.setDuration(Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		View view = LayoutInflater.from(context).inflate(R.layout.custom_toast,
				null);
		toast.setView(view);
		toast.show();
	}

	/**
	 * ��ȡ��Ƶ������ͼ
	 * ��ͨ��ThumbnailUtils������һ����Ƶ������ͼ��Ȼ��������ThumbnailUtils
	 * ������ָ����С������ͼ��
	 * �����Ҫ������ͼ�Ŀ�͸߶�С��MICRO_KIND��������Ҫʹ��MICRO_KIND
	 * ��Ϊkind��ֵ���������ʡ�ڴ档
	 * 
	 * @param videoPath
	 *            ��Ƶ��·��
	 * @param width
	 *            ָ�������Ƶ����ͼ�Ŀ��
	 * @param height
	 *            ָ�������Ƶ����ͼ�ĸ߶ȶ�
	 * @param kind
	 *            ����MediaStore.Images.Thumbnails���еĳ���MINI_KIND��MICRO_KIND�
	 *            � ���У�MINI_KIND: 512 x 384��MICRO_KIND: 96 x 96
	 * @return ָ����С����Ƶ����ͼ
	 */
	public static Bitmap getVideoThumbnail(String videoPath, int width,
			int height, int kind) {
		Bitmap bitmap = null;
		// ��ȡ��Ƶ������ͼ
		bitmap = ThumbnailUtils.createVideoThumbnail(videoPath, kind);
		System.out.println("w" + bitmap.getWidth());
		System.out.println("h" + bitmap.getHeight());
		bitmap = ThumbnailUtils.extractThumbnail(bitmap, width, height,
				ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
		return bitmap;
	}

	/**
	 * Android�ṩ��MediaMetadataRetriever����JNI(media_jni)ʵ�֡�
	 * ���ó�MediaMetadataRetriever��Ҫ����������
	 * ��MODE_GET_METADATA_ONLY��MODE_CAPTURE_FRAME_ONLY
	 * ������modeΪMODE_CAPTURE_FRAME_ONLY������captureFrameȡ��һ֡��
	 * ���⻹���������������ã� extractMetadata
	 * ��ȡ�ļ���Ϣ��ARTIST��DATE��YEAR��DURATION��RATING��FRAME_RATE��VIDEO_FORMAT
	 * ��extractAlbumArt ��ȡר����Ϣ
	 * 
	 * @param filePath
	 * @return
	 * @description ��һ�仰˵�����������ʲô
	 * @version 1.0
	 * @author ������
	 * @update 2013-11-8 ����1:11:41
	 */
	@SuppressLint("NewApi")
	public static Bitmap createVideoThumbnail(String filePath) {
		Bitmap bitmap = null;
		MediaMetadataRetriever retriever = new MediaMetadataRetriever();
		try {
			retriever.setDataSource(filePath);
			String title = retriever
					.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
			bitmap = retriever.getFrameAtTime(1000);
		} catch (IllegalArgumentException ex) {
			ex.printStackTrace();
			// Assume this is a corrupt video file
		} catch (RuntimeException ex) {
			ex.printStackTrace();
			// Assume this is a corrupt video file.
		} finally {
			try {
				retriever.release();
			} catch (RuntimeException ex) {
				// Ignore failures while cleaning up.
			}
		}
		return bitmap;
	}

}
