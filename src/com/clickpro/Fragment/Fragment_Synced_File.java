package com.clickpro.Fragment;

import java.io.FileOutputStream;
import java.util.ArrayList;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.clickpro.ccu.DownloadListActivity;
import com.clickpro.ccu.R;
import com.clickpro.models.DropBoxData;
import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.DropboxAPI.Entry;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;
import com.dropbox.client2.exception.DropboxIOException;
import com.dropbox.client2.exception.DropboxParseException;
import com.dropbox.client2.exception.DropboxPartialFileException;
import com.dropbox.client2.exception.DropboxServerException;
import com.dropbox.client2.exception.DropboxUnlinkedException;
import com.dropbox.client2.session.AccessTokenPair;
import com.dropbox.client2.session.AppKeyPair;

public class Fragment_Synced_File extends Fragment {
	ListView mSyncedFile;
	private static SharedPreferences mSharedPreferences;
	DropboxAPI<AndroidAuthSession> mApi;
	private android.widget.ImageView mImage;
	Custom adapter;
	private final String PHOTO_DIR = "/TEST/";
	// for Drop Box Authentication
	private static final String APP_KEY = "vms1kkpy97ducky";
	private static final String APP_SECRET = "878suyz710yqxtn";

	private static final String ACCOUNT_PREFS_NAME = "prefs";
	private static final String ACCESS_KEY_NAME = "ACCESS_KEY";
	private static final String ACCESS_SECRET_NAME = "ACCESS_SECRET";
	ArrayList<String> AllFile;
	ArrayList<String> path;
	static ArrayList<DropBoxData> AllFile1 = new ArrayList<DropBoxData>();
	private Context mContext;
	Fragmentsync mFragment_File_For_sync;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mContext = getActivity();
		View view = inflater.inflate(R.layout.synced_view, container, false);
		mSyncedFile = (ListView) view.findViewById(R.id.Synced);
		AndroidAuthSession session = buildSession();
		Log.e("session", "" + session.toString());
		mFragment_File_For_sync = new Fragmentsync();
		mApi = new DropboxAPI<AndroidAuthSession>(session);
		AllFile = new ArrayList<String>();
		path = new ArrayList<String>();
		AllFile1.clear();
		isNetworkAvailable(h, 2000);

		return view;
	}

	Handler h = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			if (msg.what != 1) { // code if not connected

				Log.e("Internet", "Noaccessibility");
				AlertDialog.Builder builder = new AlertDialog.Builder(
						getActivity());
				builder = new AlertDialog.Builder(getActivity());
				builder.setCancelable(false)
						.setMessage("Please Connect with Other Network")
						.setNegativeButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										getActivity().finish();
										Intent intent = new Intent(
												getActivity(),
												DownloadListActivity.class);
										startActivity(intent);
									}
								});

				AlertDialog alertDialog = builder.create();
				alertDialog.show();

			} else { // code if connected
				Log.e("Internet", "Accessibility");
				try {
					DownloadRandomPicture download = new DownloadRandomPicture(

					mContext, mApi, PHOTO_DIR);
					download.execute();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	};

	// For Check Internet Access
	public static void isNetworkAvailable(final Handler handler,
			final int timeout) {
		// ask fo message '0' (not connected) or '1' (connected) on 'handler'
		// the answer must be send before before within the 'timeout' (in
		// milliseconds)

		new Thread() {
			private boolean responded = false;

			@Override
			public void run() {
				// set 'responded' to TRUE if is able to connect with google
				// mobile (responds fast)
				new Thread() {
					@Override
					public void run() {
						HttpGet requestForTest = new HttpGet(
								"http://m.google.com");
						try {
							new DefaultHttpClient().execute(requestForTest); // can
																				// last...
							responded = true;
						} catch (Exception e) {
						}
					}
				}.start();

				try {
					int waited = 0;
					while (!responded && (waited < timeout)) {
						sleep(100);
						if (!responded) {
							waited += 100;
						}
					}
				} catch (InterruptedException e) {
				} // do nothing
				finally {
					if (!responded) {
						handler.sendEmptyMessage(0);
					} else {
						handler.sendEmptyMessage(1);
					}
				}
			}
		}.start();
	}

	private AndroidAuthSession buildSession() {
		AppKeyPair appKeyPair = new AppKeyPair(APP_KEY, APP_SECRET);

		AndroidAuthSession session = new AndroidAuthSession(appKeyPair);
		loadAuth(session);
		return session;
	}

	private void loadAuth(AndroidAuthSession session) {
		SharedPreferences prefs = this.getActivity().getSharedPreferences(
				ACCOUNT_PREFS_NAME, 0);
		String key = prefs.getString(ACCESS_KEY_NAME, null);
		String secret = prefs.getString(ACCESS_SECRET_NAME, null);
		if (key == null || secret == null || key.length() == 0
				|| secret.length() == 0)
			return;

		if (key.equals("oauth2:")) {
			// If the key is set to "oauth2:", then we can assume the token is
			// for OAuth 2.
			session.setOAuth2AccessToken(secret);
		} else {
			// Still support using old OAuth 1 tokens.
			session.setAccessTokenPair(new AccessTokenPair(key, secret));
		}
	}

	public class DownloadRandomPicture extends AsyncTask<Void, Long, Boolean> {

		private Context mContext;
		private final ProgressDialog mDialog;
		private DropboxAPI<?> mApi;
		private String mPath;
		private ImageView mView;
		private Drawable mDrawable;

		private FileOutputStream mFos;

		private boolean mCanceled;
		private Long mFileLen;
		private String mErrorMsg;
		Entry entries;

		public DownloadRandomPicture(Context context, DropboxAPI<?> api,
				String dropboxPath) {

			mContext = context;
			mApi = api;
			mPath = dropboxPath;
			mDialog = new ProgressDialog(mContext);
			mDialog.setMessage("Please Wait...");
			mDialog.setCancelable(false);
			mDialog.show();
		}

		@Override
		protected Boolean doInBackground(Void... params) {

			Log.e("Do InBackground", "Do InBackground");

			try {
				if (mCanceled) {
					return false;
				}
				Log.e("Do mPath", "" + mPath);
				// // Get the metadata for a directory
				Entry dirent = mApi.metadata(mPath, 1000, null, true, null);

				if (dirent.contents == null) {
					// It's not a directory, or there's nothing in it
					Toast.makeText(getActivity(), "Find Nothing", 2000).show();
					mErrorMsg = "File or empty directory";
					return false;
				} else {
					for (Entry ent : dirent.contents) {
						String name = ent.fileName();

						Log.e("path", "" + ent.path);
						System.out.println("My File in Folder " + name);
						AllFile.add(ent.fileName());
						path.add("/storage/sdcard0/ClickProPolar/"
								+ ent.fileName());
						DropBoxData q1 = new DropBoxData();
						q1.setName("/storage/sdcard0/ClickProPolar/"
								+ ent.fileName());
						AllFile1.add(q1);

						Log.e("Is Folder", "" + AllFile);
						Log.e("AllFile1", "" + AllFile1.toString());
					}

					return true;

				}

			} catch (DropboxUnlinkedException e) {
				// The AuthSession wasn't properly authenticated or user
				// unlinked.
			} catch (DropboxPartialFileException e) {
				// We canceled the operation
				mErrorMsg = "Download canceled";
			} catch (DropboxServerException e) {
				// Server-side exception. These are examples of what could
				// happen,
				// but we don't do anything special with them here.
				if (e.error == DropboxServerException._304_NOT_MODIFIED) {
					// won't happen since we don't pass in revision with
					// metadata
				} else if (e.error == DropboxServerException._401_UNAUTHORIZED) {
					// Unauthorized, so we should unlink them. You may want to
					// automatically log the user out in this case.
				} else if (e.error == DropboxServerException._403_FORBIDDEN) {
					// Not allowed to access this
				} else if (e.error == DropboxServerException._404_NOT_FOUND) {
					// path not found (or if it was the thumbnail, can't be
					// thumbnailed)
				} else if (e.error == DropboxServerException._406_NOT_ACCEPTABLE) {
					// too many entries to return
				} else if (e.error == DropboxServerException._415_UNSUPPORTED_MEDIA) {
					// can't be thumbnailed
				} else if (e.error == DropboxServerException._507_INSUFFICIENT_STORAGE) {
					// user is over quota
				} else {
					// Something else
				}
				// This gets the Dropbox error, translated into the user's
				// language
				mErrorMsg = e.body.userError;
				if (mErrorMsg == null) {
					mErrorMsg = e.body.error;
				}
			} catch (DropboxIOException e) {
				// Happens all the time, probably want to retry automatically.
				mErrorMsg = "Network error.  Try again.";
			} catch (DropboxParseException e) {
				// Probably due to Dropbox server restarting, should retry
				mErrorMsg = "Dropbox error.  Try again.";
			} catch (DropboxException e) {
				// Unknown error
				mErrorMsg = "Unknown error.  Try again.";
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			try {
				adapter = new Custom(getActivity(), AllFile, path);

				adapter.notifyDataSetChanged();
				mSyncedFile.setAdapter(adapter);

				mDialog.dismiss();
				Log.e("AllFile1", "" + AllFile1);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	public static ArrayList<DropBoxData> getList() {
		Log.e("AllFile1: ", "" + AllFile1);
		return AllFile1;
	}

}

class Custom extends BaseAdapter {
	private LayoutInflater inflater = null;
	private Context mContext;
	boolean my = false;
	ArrayList<String> filename1 = new ArrayList<String>();
	ArrayList<String> path1 = new ArrayList<String>();

	public Custom(FragmentActivity activity, ArrayList<String> filename,
			ArrayList<String> path) {
		// TODO Auto-generated constructor stub
		mContext = activity;
		filename1 = filename;
		path1 = path;
		inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	void add(String path) {
		filename1.add(path);
	}

	@Override
	public int getCount() {
		Log.e("getcount", "" + filename1.size());
		return filename1.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View rowView;
		Bitmap bitmap = null;
		BitmapFactory.Options options;
		rowView = inflater.inflate(R.layout.synced_item, null);

		TextView image = (TextView) rowView.findViewById(R.id.syncedFile);
		ImageView icon = (ImageView) rowView.findViewById(R.id.galleryview1);
		ImageView icon1 = (ImageView) rowView.findViewById(R.id.Videothumb1);
		if (filename1.get(position).contains(".JPG")) {
			options = new BitmapFactory.Options();
			options.inSampleSize = 8;
			bitmap = BitmapFactory.decodeFile(path1.get(position), options);
			image.setText(filename1.get(position));

		} else if (filename1.get(position).contains(".mp4")) {
			bitmap = ThumbnailUtils.createVideoThumbnail(path1.get(position),
					position);
			icon1.setVisibility(View.VISIBLE);
		}
		if (bitmap != null) {
			icon.setImageBitmap(bitmap);
		} else
			icon.setImageResource(R.drawable.icon);
		image.setText(filename1.get(position));

		return rowView;
	}

}
