package com.clickpro.Fragment;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.clickpro.ccu.R;
import com.clickpro.ccu.SelectImage;
import com.clickpro.models.DropBoxData;
import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.session.AccessTokenPair;
import com.dropbox.client2.session.AppKeyPair;

public class Fragmentsync extends Fragment {
	ListView FileForsync;
	ListView mSyncedFile;
	private static SharedPreferences mSharedPreferences;
	DropboxAPI<AndroidAuthSession> mApi;
	private android.widget.ImageView mImage;
	Custom adapter;
	private final String PHOTO_DIR = "/TEST/";
	// for Drop Box Authentication
	private static final String APP_KEY = "vms1kkpy97ducky";
	private static final String APP_SECRET = "878suyz710yqxtn";

	private static final String ACCOUNT_PREFS_NAME = "prefs";
	private static final String ACCESS_KEY_NAME = "ACCESS_KEY";
	private static final String ACCESS_SECRET_NAME = "ACCESS_SECRET";
	ArrayList<String> AllFile = new ArrayList<String>();;
	ArrayList<File> filename;
	ArrayList<String> filepath;
	ArrayList<String> temp;
	MyAdapter1 myImageAdapter;
	ArrayList<String> filename2;
	ArrayList<DropBoxData> AllFile1 = new ArrayList<DropBoxData>();
	private Context mContext;
	Fragment_Synced_File mFragment_Synced_File;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mContext = getActivity();
		View view = inflater.inflate(R.layout.for_sync_view, container, false);
		FileForsync = (ListView) view.findViewById(R.id.ForSync);
		AndroidAuthSession session = buildSession();
		Log.e("session", "" + session.toString());
		mApi = new DropboxAPI<AndroidAuthSession>(session);
		AllFile1 = Fragment_Synced_File.getList();
		for (int i = 0; i < AllFile1.size(); i++) {
			Log.e("AllFile1:", "" + AllFile1.get(i).getName());
			AllFile.add(AllFile1.get(i).getName());
		}

		try {
			filename2 = new ArrayList<String>();
			filename = new ArrayList<File>();
			filepath = new ArrayList<String>();
			temp = new ArrayList<String>();

			File targetDirector = new File("/storage/sdcard0/ClickProPolar");
			Log.e("targetDirector", "" + targetDirector);
			File[] files = targetDirector.listFiles();
			Log.e("files", "" + files);
			for (File file : files) {

				if (file.getName().equals(".dthumb")) {
					Log.e("dthumb", "dthumb");
				} else {

					filepath.add(file.getAbsolutePath());

					Log.e("filepath", "" + filepath);
				}
			}

			Log.e("AllFile", "" + AllFile);
			temp.addAll(filepath);
			temp.removeAll(AllFile);

			Log.e("temp List: ", "" + temp);
			Iterator iterator = temp.iterator();
			while (iterator.hasNext()) {
				File f = new File(iterator.next().toString());
				Log.e("Split path ", "" + f.getName());
				filename2.add(f.getName());
			}

			Log.e("sdCover List: ", "" + filename);
			myImageAdapter = new MyAdapter1(getActivity(), filename2, temp);
			myImageAdapter.notifyDataSetChanged();
			FileForsync.setAdapter(myImageAdapter);

		} catch (Exception e) {
			e.printStackTrace();
		}
		FileForsync.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				if (myImageAdapter.filename1.get(position).contains(".JPG")) {
					Intent mInDisplay = new Intent(getActivity(),
							SelectImage.class);
					mInDisplay.putExtra("Index",
							myImageAdapter.filepath1.get(position));
					startActivity(mInDisplay);
				} else {
					Intent intent = new Intent(Intent.ACTION_VIEW);
					intent.setDataAndType(
							Uri.parse(myImageAdapter.filepath1.get(position)),
							"video/mp4");
					startActivity(intent);
				}
			}
		});
		return view;

	}

	public ArrayList<File> getModifyList() {
		return filename;
	}

	private AndroidAuthSession buildSession() {
		AppKeyPair appKeyPair = new AppKeyPair(APP_KEY, APP_SECRET);

		AndroidAuthSession session = new AndroidAuthSession(appKeyPair);
		loadAuth(session);
		return session;
	}

	private void loadAuth(AndroidAuthSession session) {
		SharedPreferences prefs = this.getActivity().getSharedPreferences(
				ACCOUNT_PREFS_NAME, 0);
		String key = prefs.getString(ACCESS_KEY_NAME, null);
		String secret = prefs.getString(ACCESS_SECRET_NAME, null);
		if (key == null || secret == null || key.length() == 0
				|| secret.length() == 0)
			return;

		if (key.equals("oauth2:")) {
			// If the key is set to "oauth2:", then we can assume the token is
			// for OAuth 2.
			session.setOAuth2AccessToken(secret);
		} else {
			// Still support using old OAuth 1 tokens.
			session.setAccessTokenPair(new AccessTokenPair(key, secret));
		}

	}

	// public class DownloadRandomPicture1 extends AsyncTask<Void, Long,
	// Boolean> {
	//
	// private Context mContext;
	// private final ProgressDialog mDialog;
	// private DropboxAPI<?> mApi;
	// private String mPath;
	// private ImageView mView;
	// private Drawable mDrawable;
	//
	// private FileOutputStream mFos;
	//
	// private boolean mCanceled;
	// private Long mFileLen;
	// private String mErrorMsg;
	// Entry entries;
	//
	// public DownloadRandomPicture1(Context context, DropboxAPI<?> api,
	// String dropboxPath) {
	// // We set the context this way so we don't accidentally leak
	// // activities
	//
	// mContext = context;
	// mApi = api;
	// mPath = dropboxPath;
	// mDialog = new ProgressDialog(mContext);
	// mDialog.setMessage("Please Wait...");
	// mDialog.setCancelable(false);
	// mDialog.show();
	//
	// }
	//
	// @Override
	// protected Boolean doInBackground(Void... params) {
	//
	// Log.e("Do InBackground", "Do InBackground");
	//
	// try {
	// if (mCanceled) {
	// return false;
	// }
	// Log.e("Do mPath", "" + mPath);
	// // // Get the metadata for a directory
	// Entry dirent = mApi.metadata(mPath, 1000, null, true, null);
	//
	// if (!dirent.isDir || dirent.contents == null) {
	// // It's not a directory, or there's nothing in it
	//
	// mErrorMsg = "File or empty directory";
	// return false;
	// } else {
	// for (Entry ent : dirent.contents) {
	// String name ="/storage/sdcard0/ClickProPolar/"+ ent.fileName();
	//
	// System.out.println("My File in Folder " + name);
	// AllFile.add(name);
	// Log.e("Is Folder", "" + AllFile);
	// }
	//
	// return true;
	//
	// }
	//
	// } catch (DropboxUnlinkedException e) {
	// // The AuthSession wasn't properly authenticated or user
	// // unlinked.
	// } catch (DropboxPartialFileException e) {
	// // We canceled the operation
	// mErrorMsg = "Download canceled";
	// } catch (DropboxServerException e) {
	// // Server-side exception. These are examples of what could
	// // happen,
	// // but we don't do anything special with them here.
	// if (e.error == DropboxServerException._304_NOT_MODIFIED) {
	// // won't happen since we don't pass in revision with
	// // metadata
	// } else if (e.error == DropboxServerException._401_UNAUTHORIZED) {
	// // Unauthorized, so we should unlink them. You may want to
	// // automatically log the user out in this case.
	// } else if (e.error == DropboxServerException._403_FORBIDDEN) {
	// // Not allowed to access this
	// } else if (e.error == DropboxServerException._404_NOT_FOUND) {
	// // path not found (or if it was the thumbnail, can't be
	// // thumbnailed)
	// } else if (e.error == DropboxServerException._406_NOT_ACCEPTABLE) {
	// // too many entries to return
	// } else if (e.error == DropboxServerException._415_UNSUPPORTED_MEDIA) {
	// // can't be thumbnailed
	// } else if (e.error == DropboxServerException._507_INSUFFICIENT_STORAGE) {
	// // user is over quota
	// } else {
	// // Something else
	// }
	// // This gets the Dropbox error, translated into the user's
	// // language
	// mErrorMsg = e.body.userError;
	// if (mErrorMsg == null) {
	// mErrorMsg = e.body.error;
	// }
	// } catch (DropboxIOException e) {
	// // Happens all the time, probably want to retry automatically.
	// mErrorMsg = "Network error.  Try again.";
	// } catch (DropboxParseException e) {
	// // Probably due to Dropbox server restarting, should retry
	// mErrorMsg = "Dropbox error.  Try again.";
	// } catch (DropboxException e) {
	// // Unknown error
	// mErrorMsg = "Unknown error.  Try again.";
	// }
	// return false;
	// }
	//
	// @Override
	// protected void onPostExecute(Boolean result) {
	// try {
	//
	//
	//
	//
	// mDialog.dismiss();
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	//
	// }

	class MyAdapter1 extends BaseAdapter {
		private LayoutInflater inflater = null;
		private Context mContext;
		boolean my = false;
		ArrayList<String> filename1 = new ArrayList<String>();
		ArrayList<String> filepath1 = new ArrayList<String>();

		public MyAdapter1(FragmentActivity activity, ArrayList<String> temp,
				ArrayList<String> filepath) {
			// TODO Auto-generated constructor stub
			mContext = activity;
			filename1 = temp;
			filepath1 = filepath;
			inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		void add(String path) {
			filename1.add(path);
		}

		@Override
		public int getCount() {
			Log.e("getcount", "" + filename1.size());
			return filename1.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View rowView;
			Bitmap bitmap = null;
			BitmapFactory.Options options;
			rowView = inflater.inflate(R.layout.for_sync_item, null);
			TextView image = (TextView) rowView.findViewById(R.id.SdCard_item);
			ImageView icon = (ImageView) rowView.findViewById(R.id.galleryview);
			ImageView icon1 = (ImageView) rowView.findViewById(R.id.Videothumb);
			final CheckBox select = (CheckBox) rowView.findViewById(R.id.check);
			if (filename1.get(position).contains(".JPG")) {
				options = new BitmapFactory.Options();
				options.inSampleSize = 8;
				bitmap = BitmapFactory.decodeFile(filepath1.get(position),
						options);

			} else if (filename1.get(position).contains(".mp4")) {

				bitmap = ThumbnailUtils.createVideoThumbnail(
						filepath1.get(position), position);
				icon1.setVisibility(View.VISIBLE);
			}

			image.setText(filename1.get(position));
			icon.setImageBitmap(bitmap);
			select.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (select.isChecked()) {

						filename.add(new File(filepath1.get(position)));
						Log.e("filename", "" + filename);
					} else if (!select.isChecked()) {

						filename.remove(new File(filepath1.get(position)));
						Log.e("filename", "" + filename);
					}
				}
			});

			return rowView;
		}

	}
}