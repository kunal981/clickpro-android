/**
 * 
 */
package com.clickpro.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.AttributeSet;
import android.view.View;

/**
 * @description
 * @version 1.0
 * @author ������
 * @update 2013-12-17 ����11:59:00
 */

public class MyView extends View {
	Context mContext;
	Bitmap bitmap;
	int sWidth;

	public MyView(Context mContext) {
		super(mContext);
		this.mContext = mContext;
	}

	public MyView(Context mContext, AttributeSet attr) {
		super(mContext, attr);
		this.mContext = mContext;
	}

	public void setBitmap(Bitmap bitmap) {
		this.bitmap = bitmap;
	}

	public void setWidth(int sWidth) {
		this.sWidth = sWidth;
	}

	@SuppressLint("DrawAllocation")
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		if (bitmap != null) {
			int width = bitmap.getWidth();
			float zoomScale = ((float) sWidth) / width;
			Matrix matrix = new Matrix();
			matrix.postScale(zoomScale, zoomScale);
			canvas.drawBitmap(bitmap, matrix, null);
		}

	}

}
