package com.clickpro.view;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.clickpro.util.Global;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class MySurfaceView extends SurfaceView implements
		SurfaceHolder.Callback {
	SurfaceHolder surfaceHolder;
	public static boolean isRun = false;
	private Context mContext;
	private float width;
	Bitmap localBitmap;
	float scale;
	Matrix matrix = new Matrix();
	Paint paint = new Paint();
	ChangeSizeListener listener;
	Canvas canvas;

	public void setChangeSizeListener(ChangeSizeListener listener) {
		this.listener = listener;
	}

	public MySurfaceView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		surfaceHolder = this.getHolder();
		surfaceHolder.addCallback(this);
		this.mContext = context;
		width = ((Activity) context).getWindowManager().getDefaultDisplay()
				.getWidth();
	}

	public MySurfaceView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		surfaceHolder = this.getHolder();
		surfaceHolder.addCallback(this);
		this.mContext = context;
		width = ((Activity) context).getWindowManager().getDefaultDisplay()
				.getWidth();
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
		isRun = true;
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		reCreate();
		System.out.println("");
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		isRun = false;
		System.out.println("");
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// TODO Auto-generated method stub
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);

	}

	public void reCreate() {
		new GetImageThread().start();
	}

	class GetImageThread extends Thread {

		public void run() {
			// TODO Auto-generated method stub
			try {
				DefaultHttpClient httpClient = new DefaultHttpClient();
				HttpGet httpGet;
				HttpResponse localHttpResponse;

				while (true) {

					Log.e("",
							"�߳�-->"
									+ Thread.currentThread().getName()
									+ "---"
									+ new SimpleDateFormat("hh:mm:ss:SS")
											.format(new Date()));
					if (isRun) {
						String url = "http://192.168.42.1/mjpeg/amba.jpg?ts="
								+ new Date().getTime();
						httpGet = new HttpGet(url);

						localHttpResponse = httpClient.execute(httpGet);
						if (localHttpResponse.getStatusLine().getStatusCode() == 200) {
							Global.isConn = true;
							localBitmap = BitmapFactory
									.decodeStream(localHttpResponse.getEntity()
											.getContent());

							if (localBitmap != null) {
								if (scale == 0) {
									float bmpWidth = localBitmap.getWidth();
									scale = width / bmpWidth;
									matrix.postScale(scale, scale);
									// listener.changeSize(scale,
									// localBitmap.getHeight());
								}
								canvas = surfaceHolder.lockCanvas();
								if (canvas != null) {
									canvas.drawBitmap(localBitmap, matrix,
											paint);
									surfaceHolder.unlockCanvasAndPost(canvas);
								}
							}
						} else {
							Global.isConn = false;
						}

					} else {
						return;
					}

				}

			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				Global.isConn = false;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				Global.isConn = false;
			}

		}
	}

	public interface ChangeSizeListener {
		public void changeSize(float scale, int bmpHeight);
	}

}
